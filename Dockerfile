FROM golangci/golangci-lint:latest-alpine

RUN go get -u github.com/reviewdog/reviewdog/cmd/reviewdog
