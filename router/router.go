package router

import (
	"github.com/astaxie/beego"
)
import "minishop_imooc_backend/controller"

func init() {
	//微信小程序首页轮播图接口
	beego.Router("/Api/indexSwiper", &controller.ShopController{}, "*:GetSwiperAction")

	//微信小程序首页热销排行榜接口
	beego.Router("/Api/hotProductList", &controller.ShopController{}, "*:GetHotProductListAction")

	//个人中心，热门推荐，使用热门商品，具体按自身的业务来决定使用哪些条件
	beego.Router("/Api/ucenterRecommendList", &controller.ShopController{}, "*:GetHotProductListAction")

	//个人中心---我的订单
	beego.Router("/Api/ucenterOrderList", &controller.ShopController{}, "*:UcenterOrderListAction")


	//个人中心---我的订单---订单详情
	beego.Router("/Api/ucenterOrderDetail", &controller.ShopController{}, "*:UcenterOrderDetailAction")

	//个人中心---我的订单---取消订单
	beego.Router("/Api/cancleOrder", &controller.ShopController{}, "*:CancleOrderAction")

	//个人中心---我的收货地址
	beego.Router("/Api/myAddress", &controller.ShopController{}, "*:MyAddressAction")

	//个人中心---设置我的默认收货地址
	beego.Router("/Api/setDefaultAddress", &controller.ShopController{}, "*:SetDefaultAddressAction")

	//个人中心---删除我的指定收货地址
	beego.Router("/Api/delAddress", &controller.ShopController{}, "*:DelAddressAction")

	//个人中心---收货地址-添加收货地址-省份,市，区
	beego.Router("/Api/map", &controller.ShopController{}, "*:MapAction")

	//个人中心---收货地址-添加收货地址-入库
	beego.Router("/Api/addAddress", &controller.ShopController{}, "*:AddAddressAction")

	//个人中心---我的收藏
	beego.Router("/Api/getMyCollect", &controller.ShopController{}, "*:GetMyCollectAction")

	//个人中心---我的收藏-取消收藏
	//beego.Router("/Api/cancleCollect", &controller.ShopController{}, "*:CancleCollectAction")

	//个人中心---优惠券列表
	beego.Router("/Api/couponList", &controller.ShopController{}, "*:CouponListAction")

	//个人中心---优惠券领取
	beego.Router("/Api/getCoupon", &controller.ShopController{}, "*:GetCouponAction")

	//个人中心，我的权益---我领取的优惠券
	beego.Router("/Api/getMyCoupon", &controller.ShopController{}, "*:GetMyCouponAction")



	//微信小程序首页收藏排行榜接口
	beego.Router("/Api/collectProductList", &controller.ShopController{}, "*:GetCollectProductListAction")

	//微信小程序首页新品发布接口
	beego.Router("/Api/newProductList", &controller.ShopController{}, "*:GetNewProductListAction")

	//微信小程序搜索页接口
	beego.Router("/Api/search", &controller.ShopController{}, "*:SearchAction")
	//beego.Router("/Api/search", &controller.ShopController{}, "*:SearchProductByKeyword")

	//微信小程序分类页接口
	beego.Router("/Api/categoryList", &controller.ShopController{}, "*:CategoryListAction")

	//微信小程序分类下的商品列表页接口
	beego.Router("/Api/listProduct", &controller.ShopController{}, "*:ListProductAction")

	//微信小程序商品详情页接口
	beego.Router("/Api/productDetail", &controller.ShopController{}, "*:ProductDetailAction")
	beego.Router("/Api/getProductDetail", &controller.ShopController{}, "*:ProductDetailAction")

	//微信小程序商品详情页添加到购物车接口
	beego.Router("/Api/productToCarts", &controller.ShopController{}, "*:ProductToCartsAction")


	//浏览足迹页
	beego.Router("/Api/getProductView", &controller.ShopController{}, "*:ProductViewAction")

	//微信小程序商品详情页商品的收藏与取消收藏接口
	beego.Router("/Api/collection", &controller.ShopController{}, "*:CollectionAction")

	//分类页----根据一级分类获取二级分类
	beego.Router("/Api/getSwiperHotProduct", &controller.ShopController{}, "*:SwiperHotProductAction")


	//微信小程序购物车接口
	beego.Router("/Api/myCartList", &controller.ShopController{}, "*:MyCartListAction")
	beego.Router("/Api/getCartList", &controller.ShopController{}, "*:MyCartListAction")

	//微信小程序预下单接口
	beego.Router("/Api/CartsToBuy", &controller.ShopController{}, "*:CartsToBuyAction")

	//微信小程序预下单发起支付请求接口
	beego.Router("/Api/orderToPrepay", &controller.ShopController{}, "*:OrderToPrepayAction")
	beego.Router("/Api/orderToPay", &controller.ShopController{}, "*:OrderToPayAction")


	//微信小程序获取openid和session_key接口
	beego.Router("/Api/getAuthData", &controller.ShopController{}, "*:GetAuthDataAction")

	//微信小程序注册接口接口
	beego.Router("/Api/userReg", &controller.ShopController{}, "*:UserRegAction")

	beego.Router("/Api/authReg", &controller.ShopController{}, "*:UserRegAction")

	//微信小程序支付接口
	beego.Router("/Api/miniPay", &controller.ShopController{}, "*:MiniPayAction")

	//微信小程序支付接口
	beego.Router("/Api/callback", &controller.ShopController{}, "*:CallbackAction")

	beego.Router("/Api/DbDeal", &controller.ShopController{}, "*:DbDealAction")


}
