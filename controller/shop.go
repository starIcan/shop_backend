package controller

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"github.com/astaxie/beego"
	"github.com/astaxie/beego/logs"
	"github.com/codersay/MiniPay"
	_ "github.com/go-sql-driver/mysql"
	"github.com/jmoiron/sqlx"
	"io/ioutil"
	"log"
	"minishop_imooc_backend/common"
	"minishop_imooc_backend/conf"
	"strings"
	"time"
)

type ShopController struct {
	beego.Controller
}

var (
	shopConfig    = conf.InitAppConfig()
	executeResult map[string]interface{} //执行结果
	msg           string                 //执行结果-描述
	status        int                    //执行结果-状态码 0表示失败 1表示成功
	type_id       int                    //是否推荐 1推荐 0不推荐
	limit         int                    //获取的记录数select * from xx limit x或分页时起始的记录数 select * from xx limit x,y 指这里的x
	limits     int                    //limit x,y 这里的y
	photoLists     []conf.PhotoLists      //缩略图
	Db                          *sqlx.DB
	database                    *sqlx.DB
	err                         error
	photoList                   []conf.PhotoList    //首页轮播图
	product                     []conf.Product      //商品相关的切片
	uid                         int                 //当前登陆用户的id
	pro_id                      int                 //当前获取的商品id
	searchLimit                 int                 //登陆用户最近搜索的条数或者热门搜索条数
	searchList                  []conf.SearchList   //最近搜索相关的切片
	keyword                     string              //搜索的关键词
	page                        int                 //当前的页数
	pagesize                    int                 //每页展示的记录数
	categoryList                []conf.CategoryList //分类相关
	cat_id                      int                 //存储分类id
	catelimit                   int                 //微信客户端传递过来的分类的记录数
	catesize                    int                 //分类展示的默认记录数
	category_recommend_size     int                 //分类页上方轮播图记录数
	category_recommend_hot_size int                 //分类页热销商品推荐
	listproduct_size            int                 //分类页下的商品列表页展示的记录数
	collection                  []conf.Collection   //商品收藏
	productView 				[]*conf.ProductView //浏览足迹
	num                         int                 //购物车商品数量
	carts                       []conf.Carts        //购物车
	userCoupon                  []conf.UserCoupon   //我的优惠券
	couponDetail                []conf.CouponDetail //优惠券详情
	productCarts                []conf.ProductCarts //预下单页商品情况
	cart_ids                    string              //购物车传递给服务端购物车的主键，通过逗号分隔
	address                     []conf.Address      //收货地址
	issearch   int64                  //是否是搜索逻辑
	searchlist []*conf.SearchList //搜索关键词
	price               float64 //商品的价格
	minusPrice          float64 //使用优惠券之后的价格
	couponid            int     //优惠券Id
	proNum              int     //商品总数量
	remark              string  //备注
	address_detail      string  // 详细的收货地址
	tel                 string  //联系方式
	concat              string  //联系人
	code                string  //code
	appid               string
	appsecret           string
	httpsclient         common.HTTPSClient
	openidAndSessionKey = &conf.OpenIdAndSessionKey{}
	user                []conf.User
	unifiedData         map[string]interface{}
	order        []*conf.Order        //订单表
	orderProduct []*conf.OrderProduct //订单详情表
	paystatus  int64                  //订单状态
	totalRow []*conf.TotalRow //分页时候的总记录数
	id int64

	myCollect      []*conf.MyCollect      //我的收藏
	maps    	   []*conf.Map     //城市表
	coupon 		   []*conf.Coupon
	coupid     int64                  //优惠券id
	point      int64                  //用户所拥有的积分
	coupon_del    int //优惠券的删除状态  0正常 1删除
	coupon_status int //优惠券的状态 1未使用 2已使用 3已失效
	myUserCoupon []*conf.MyUserCoupon //我的优惠券
	usercoupon   []*conf.UserCoupon   //用户优惠券
	cart_id int64 //立即购买添加到购物车之后，返回操作的id，然后再跳转
)

func init() {

	if database, err = sqlx.Open(shopConfig.DbDriver, shopConfig.DbUser+":"+shopConfig.DbPwd+"@"+shopConfig.DbProtocol+"("+shopConfig.DbHost+":"+shopConfig.DbPort+")/"+shopConfig.DbDatabase); err != nil {
		logs.Debug("数据库连接失败", err.Error())
	}
	logs.Debug("数据库连接成功")
	Db = database
	executeResult = make(map[string]interface{})
	unifiedData = make(map[string]interface{})
	searchLimit = 5
	pagesize = 10
	catesize = 10
	category_recommend_size = 5
	category_recommend_hot_size = 10
	listproduct_size = 20
}
//func init111(){
//	if database,err = sqlx.Open(shopConfig.DbDriver,shopConfig.DbUser+":"+shopConfig.DbPwd+"@"+shopConfig.DbProtocol+"("+shopConfig.DbHost+":"+shopConfig.DbPort+")/"+shopConfig.DbDatabase); err!=nil{
//		logs.Debug("数据库连接失败"，err.Error())
//	}
//	logs.Debug("数据库连接成功")
//	Db = database
//	executeResult = make(map[string]interface{})
//	unifiedData = make(map[string]interface{})
//	searchLimit = 5
//	pagesize = 10
//	catesize = 10
//	category_recommend_size = 5
//	category_recommend_hot_size = 10
//	listproduct_size = 20
//}
func (this *ShopController) GetSwiperAction() {
	executeResult = make(map[string]interface{})
	if type_id, err = this.GetInt("type"); err != nil {
		type_id = 1

	}

	if limit, err = this.GetInt("limit"); err != nil {
		limit = 5
	}
	logs.Debug("select id,name,photo_big from "+shopConfig.TablePre+"product where `type` = ? order by id desc limit ?", type_id, limit)
	photoList = nil
	if err = Db.Select(&photoList, "select id,name,photo_big from "+shopConfig.TablePre+"product where `type` = ? order by id desc limit ?", type_id, limit); err != nil {
		executeResult["status"] = 0
		executeResult["msg"] = "数据查询异常" + err.Error()
		logs.Debug("hjffjghjffjh")
		this.Data["json"] = executeResult
		this.ServeJSON()
		return
	}
	executeResult["status"] = 1
	executeResult["msg"] = "数据获取成功"
	executeResult["data"] = photoList
	this.Data["json"] = executeResult
	this.ServeJSON()
}
func (this *ShopController) GetSwiperAction11(){
	executeResult = make(map[string]interface{})
	if type_id,err = this.GetInt("type"); err!=nil{
		type_id = 1
	}
	if limit,err = this.GetInt("limit");err !=nil{
		limit = 5
	}
	logs.Debug("select id,name,photo_big from"+shopConfig.TablePre+"product where `type` = ? order by id desc limit ?",type_id,limit)
	photoList = nil
	if err = Db.Select(&photoList,"select id,name,photo_big from"+shopConfig.TablePre+"product where `type` = ? order by id desc limit ?",type_id,limit);err != nil{
		executeResult["status"] = 0
		executeResult["msg"] = "数据查询异常"+err.Error()
		this.Data["json"] = executeResult
		this.ServeJSON()
		return
	}
	executeResult["status"] = 1
	executeResult["msg"] = "数据获取成功"
	executeResult["data"] = photoList
	this.Data["json"] = executeResult
	this.ServeJSON()
}
//个人中心，订单
//订单表，status,当前订单的支付状态，1未付款  2待发货 3待收货 4交易完成 5退款退货申请 6退款退货完成或者说订单关闭，在录课的时候，需要删除0初始化这个说明
func (this *ShopController) UcenterOrderListAction() {
	//如果我们要删除map的所有的key,没有专门的方法一次删除，可以遍历key来删除或者 map=make(...),make一个新的，让原来的成为垃圾被gc回收
	executeResult = make(map[string]interface{})

	if uid, err = this.GetInt("user_id"); err != nil || uid == 0 {
		executeResult["status"] = 0
		executeResult["msg"] = "用户异常"
		this.Data["json"] = executeResult
		this.ServeJSON()
		return
	}

	//if uid, err = this.GetInt("user_id"); err != nil || uid==0{
	//	executeResult["status"] = 0
	//	executeResult["msg"] = "用户异常"
	//	this.Data["json"] = executeResult
	//	this.ServeJSON()
	//	return
	//}

	if page, err = this.GetInt("p"); err != nil || page == 0 {
		//页数有误，强制第一页
		page = 1
	}

	//if page,err = this.GetInt("p");err != nil || page == 0{
	//	page = 1
	//}

	//带分页时候的记录数
	limit = (page - 1) * pagesize
	del := 0 //订单状态 0正常 1删除，这里不要写si,待需要变更时，可以灵活配置,存储到配置文件，功能完成再添加去

	//当前订单的支付状态，1未付款  2已付款待发货 3已发货待收货 4收到货交易完成 【5退款退货申请  6退款退货完成或者说订单关闭 这两种条件，各位同学可以根据课程当中老师所讲解的微信支付的逻辑，把微信小程序的退款接口给实现，同时把这里的5，6，这两个条件给加上去】
	if paystatus, err = this.GetInt64("pay_status" ); err != nil || paystatus < 0 {
		executeResult["status"] = 0
		executeResult["msg"] = "订单状态异常"

		this.Data["json"] = executeResult
		this.ServeJSON()
		return
	}
	//if paystatus,err = this.GetInt64("pay_status"); err != nil || paystatus < 0 {
	//	executeResult["status"] = 0
	//	executeResult["mag"] = "订单状态异常"
	//	this.Data["Json"] = executeResult
	//	this.ServeJSON()
	//	return
	//}
	order = nil
	if paystatus == 0 {
		err = Db.Select(&totalRow, "select count(1) as total from `"+shopConfig.TablePre+"order` where `uid` = ? and `del` = ? ", uid, del)
		err = Db.Select(&order, "select `id`,`order_sn`,`trade_no`,`status`,`price`,`price_real_pay`,`product_num` from `"+shopConfig.TablePre+"order` where `uid` = ? and `del` = ? order by id desc limit ?,?", uid, del, limit, pagesize)
	} else {
		err = Db.Select(&totalRow, "select count(1) as total from `"+shopConfig.TablePre+"order` where `uid` = ? and `del` = ? and `status` = ?", uid, del, paystatus)
		err = Db.Select(&order, "select `id`,`order_sn`,`trade_no`,`status`,`price`,`price_real_pay`,`product_num` from `"+shopConfig.TablePre+"order` where `uid` = ? and `del` = ? and `status` = ? order by id desc limit ?,?", uid, del, paystatus, limit, pagesize)
	}

	if err != nil {
		executeResult["status"] = 0
		executeResult["msg"] = "订单异常"
		this.Data["json"] = executeResult
		this.ServeJSON()
		return
	}

	orderProduct = nil
	var cartData map[string]interface{}
	cartData = make(map[string]interface{})

	for _, v := range order {
		err = Db.Select(&orderProduct, "select `id`,`pid`,`order_id`,`name`,`price`,`photo_little`,`num` from `"+shopConfig.TablePre+"order_product` where `order_id` = ?", v.Id)
		if len(orderProduct) >0 {
			v.PhotoLittle = orderProduct[0].Photo_little
		} else {
			v.PhotoLittle = ""
		}

		orderProduct = nil
	}

	//订单详情表中的数据，每个订单表中的商品对应多个订单详情表中的数据，取第一个订单详情来展示，比如展示缩略图
	executeResult["orderList"] = cartData
	//订单表中的数据
	executeResult["order"] = order

	this.Data["json"] = executeResult
	this.ServeJSON()
}

func (this *ShopController) UcenterOrderListAction111() {
	executeResult = make(map[string]interface{})


}

func (this *ShopController) GetHotProductListAction() {
	executeResult = make(map[string]interface{})
	if limit, err = this.GetInt("limit"); err != nil {
		limit = 10
	}

	product = nil
	if err = Db.Select(&product, "select id,intro,name,photo_little,price,price_preferential,is_new,is_hot,saledcount from "+shopConfig.TablePre+"product order by saledcount desc limit ?", limit); err != nil {
		executeResult["status"] = 0
		executeResult["msg"] = "数据查询异常" + err.Error()
		this.Data["json"] = executeResult
		this.ServeJSON()
		return
	}

	executeResult["status"] = 1
	executeResult["msg"] = "数据获取成功"
	executeResult["data"] = product
	this.Data["json"] = executeResult
	this.ServeJSON()
}

func (this *ShopController) GetCollectProductListAction() {
	executeResult = make(map[string]interface{})
	if limit, err = this.GetInt("limit"); err != nil {
		limit = 10
	}

	product = nil
	if err = Db.Select(&product, "select id,intro,name,photo_little,price,price_preferential,is_new,is_hot,saledcount from "+shopConfig.TablePre+"product where id in (select id from (select p.id,count(p.id) as total from "+shopConfig.TablePre+"product p join "+shopConfig.TablePre+"product_collection c on p.id = c.pid group by p.id order by total desc limit ?) t );", limit); err != nil {
		executeResult["status"] = 0
		executeResult["msg"] = "数据查询异常" + err.Error()
		this.Data["json"] = executeResult
		this.ServeJSON()
		return
	}

	executeResult["status"] = 1
	executeResult["msg"] = "数据获取成功"
	executeResult["data"] = product
	this.Data["json"] = executeResult
	this.ServeJSON()
}

func (this *ShopController) GetNewProductListAction() {
	executeResult = make(map[string]interface{})
	if limit, err = this.GetInt("limit"); err != nil {
		limit = 10
	}

	product = nil
	if err = Db.Select(&product, "select id,intro,name,photo_little,price,price_preferential,is_new,is_hot,saledcount from "+shopConfig.TablePre+"product order by addtime asc limit ?", limit); err != nil {
		executeResult["status"] = 0
		executeResult["msg"] = "数据查询异常" + err.Error()
		this.Data["json"] = executeResult
		this.ServeJSON()
		return
	}

	executeResult["status"] = 1
	executeResult["msg"] = "数据获取成功"
	executeResult["data"] = product
	this.Data["json"] = executeResult
	this.ServeJSON()
}

func (this *ShopController) SearchAction() {
	executeResult = make(map[string]interface{})
	keyword = this.GetString("keyword")
	if keyword == "" {
		//根据业务需求进行业务处理
	}
	fmt.Println(keyword)
	if uid, err = this.GetInt("uid"); err != nil {
		//允许用户不登陆也可以查询
	}

	if keyword != "" {
		if page, err = this.GetInt("p"); err != nil {
			page = 1
		}

		limit = (page - 1) * pagesize

		//根据关键词进行搜索

		searchKeySql := fmt.Sprintf("select id,intro,cid,name,photo_little,photo_lists,price,price_preferential,saledcount,brand_id from `"+shopConfig.TablePre+"product` where name like %s%s%s  order by id desc limit %d,%d",   "'%",keyword,"%'", limit, pagesize)

		logs.Debug(searchKeySql)
		product = nil
		if err = Db.Select(&product, searchKeySql); err != nil {
			executeResult["status"] = 0
			executeResult["msg"] = "数据不存在"
			this.Data["json"] = executeResult
			this.ServeJSON()
		}

		if len(product) > 0 {
			executeResult["searchpro"] = nil
			executeResult["searchpro"] = product

			searchList = nil
			if err = Db.Select(&searchList, "select keyword,uid from "+shopConfig.TablePre+"search_history where uid=? and keyword=?", uid, keyword);err != nil {
				log.Println(err.Error())
			}

			if len(searchList) == 0 {
				_, err := Db.Exec("insert into "+shopConfig.TablePre+"search_history(uid,keyword,num,addtime) values(?,?,?,?)", uid, keyword, 1, time.Now().Unix())

				if err != nil {
					executeResult["insertStatus"] = 0
				} else {
					executeResult["insertStatus"] = 1
				}
			} else {
				_, err := Db.Exec("update "+shopConfig.TablePre+"search_history set num=num+1 where uid = ? and keyword=?", uid, keyword)
				if err != nil {
					executeResult["updateStatus"] = 0
				} else {
					executeResult["updateStatus"] = 1
				}
			}
		}
	}

	//最近搜索
	if uid > 0 {
		searchList = nil
		if err = Db.Select(&searchList, "select keyword from "+shopConfig.TablePre+"search_history where uid = ? order by addtime desc limit ?", uid, searchLimit); err != nil {
			//允许用户登陆的前提下，这里的最近搜索为空
		}

		executeResult["recentkey"] = searchList
	}

	//热门搜索
	searchList = nil
	if err = Db.Select(&searchList, "select  keyword,sum(num) as num from "+shopConfig.TablePre+"search_history group by keyword order by num desc limit ?", searchLimit); err != nil {
		//热门搜索为空的时候，我们不做任何处理
	}

	if len(searchList) > 0 {
		executeResult["hotkey"] = searchList
	}

	executeResult["status"] = 1
	executeResult["msg"] = "数据获取成功"
	this.Data["json"] = executeResult
	this.ServeJSON()
}


//搜索页
//两个逻辑均走这里
//1.默认展示热门搜索和当前登陆用户最近的搜索
//2.当输入关键词时，上面的隐藏，显示是否有关键词对应的商品，将当前搜索的关键词记录数+1，如果有展示，没有则提示不存在，需要添加这条关键词到表中
func (this *ShopController) SearchProductByKeyword() {
	//如果我们要删除map的所有的key,没有专门的方法一次删除，可以遍历key来删除或者 map=make(...),make一个新的，让原来的成为垃圾被gc回收
	executeResult = make(map[string]interface{})

	//输入关键词
	keyword = this.GetString("keyword" )
	if keyword == "" {
	}
	issearch, err = this.GetInt64("issearch" )
	//这里如果出错也不处理，因为默认是不存在的，因为默认走的是1逻辑

	//当前登陆的用户id,未登陆不允许查询，具体看你们公司的需求
	if uid, err = this.GetInt("uid" ); err != nil {
		executeResult["status"] = 0
		executeResult["msg"] = "用户未登录" //实际业务中，不要暴露具体业务

		this.Data["json"] = executeResult
		this.ServeJSON()
		return
	}

	if page, err = this.GetInt("p" ); err != nil {
		//如果不传递，则默认为第一页
		page = 1
	}

	//带分页时候的记录数
	limit = (page - 1) * pagesize

	fmt.Println(keyword)
	fmt.Println(issearch)
	if keyword != "" && issearch > 0 {
		searchTop := fmt.Sprintf("select id,intro,cid,name,photo_little,photo_lists,price,price_preferential,saledcount,brand_id from `"+shopConfig.TablePre+"product` where name like '%s%s%s'  order by id desc limit %d,%d",   "%",keyword,"%", limit, pagesize)
		fmt.Println(searchTop)
		product = nil
		if err = Db.Select(&product, searchTop); err != nil {
			executeResult["status"] = 0
			executeResult["msg"] = "未找到对应的商品" + err.Error() //实际业务中，不要暴露具体业务  //msg = "数据异常" + err.Error()

			this.Data["json"] = executeResult
			this.ServeJSON()
			return
		}

		fmt.Println("product",product)

		//默认没有商品
		executeResult["searchpro"] = nil
		if len(product) > 0 {

			searchlist = nil
			if err = Db.Select(&searchlist, "select `keyword`,`uid` from `"+shopConfig.TablePre+"search_history` where uid = ?", uid); err != nil {
				//未查询到
				fmt.Println("Db.Select(&searchlist, \"select `keyword`",err.Error())
			}
			fmt.Println("searchlist",searchlist,len(searchlist))
			if len(searchlist) == 0 {
				_, err := Db.Exec("insert into `"+shopConfig.TablePre+"search_history`(`uid`, `keyword`, `num`,`addtime`)values(?,?,?,?)", uid, keyword, 1, time.Now().Unix())
				if err != nil {
					executeResult["upnum"] = 0 //更新失败，我们并不关注这些小的点，所以不需要报错，可以增加字段，用于调试使用
					msg = err.Error()
					status = 0
				} else {
					executeResult["upnum"] = 1 //更新成功，我们并不关注这些小的点，所以不需要报错，可以增加字段，用于调试使用
					msg = "添加成功"
					status = 1
				}
			}

			//通过关键词查找到的商品
			executeResult["searchpro"] = product

			if _, err := Db.Exec("update `"+shopConfig.TablePre+"search_history` set num=num+1 where `keyword` = ? and uid=?", keyword, uid); err != nil {
				executeResult["upnum"] = 0 //更新失败，我们并不关注这些小的点，所以不需要报错，可以增加字段，用于调试使用
				status = 0
				msg = "操作异常,请稍候再试" //不使用将上面具体的业务信息打印出来，而是给到一个宽范的描述
			} else {
				executeResult["upnum"] = 1 //更新成功，我们并不关注这些小的点，所以不需要报错，可以增加字段，用于调试使用
				status = 1
				msg = "操作成功"
			}
		} else {
			_, err := Db.Exec("insert into `"+shopConfig.TablePre+"search_history`(`uid`, `keyword`, `num`,`addtime`)values(?,?,?,?)", uid, keyword, 1, time.Now().Unix())
			if err != nil {
				executeResult["upnum"] = 0 //更新失败，我们并不关注这些小的点，所以不需要报错，可以增加字段，用于调试使用
				msg = err.Error()
				status = 0
			} else {
				executeResult["upnum"] = 1 //更新成功，我们并不关注这些小的点，所以不需要报错，可以增加字段，用于调试使用
				msg = "添加成功"
				status = 1
			}
		}
	} else {
		//热门搜索
		searchlist = nil
		if err = Db.Select(&searchlist, "select `keyword`,sum(num) as num from `"+shopConfig.TablePre+"search_history` group by keyword order by num desc limit 10"); err != nil {
			fmt.Println(err.Error())
		}
		executeResult["hotkey"] = searchlist

		//当前用户最近的搜索
		searchlist = nil
		if err  = Db.Select(&searchlist, "select `keyword` from `"+shopConfig.TablePre+"search_history` where uid=? order by addtime desc limit ?", uid, limits); err != nil {
			fmt.Println(err.Error())
		}
		this.Data["json"] = executeResult
		this.ServeJSON()
	}

	executeResult["status"] = 1
	executeResult["page"] = page
	executeResult["msg"] = "ok" //实际业务中，不要暴露具体业务

	this.Data["json"] = executeResult
	this.ServeJSON()
	return
}

func (this *ShopController) CategoryListAction() {
	executeResult = make(map[string]interface{})
	if cat_id, err = this.GetInt("cat_id"); err != nil {
		executeResult["status"] = 0
		executeResult["msg"] = "参数异常" + err.Error()
		this.Data["json"] = executeResult
		this.ServeJSON()
	}

	if catelimit, err = this.GetInt("limit"); err != nil {
		catelimit = catesize
	}

	categoryList = nil
	if err = Db.Select(&categoryList, "select id,name,preview,tid from "+shopConfig.TablePre+"category where tid=? limit ?", cat_id, catelimit); err != nil {
		executeResult["status"] = 0
		executeResult["msg"] = "数据异常" + err.Error()
		this.Data["json"] = executeResult
		this.ServeJSON()
	}

	photoList = nil
	if err = Db.Select(&photoList, "select id,name,photo_big from "+shopConfig.TablePre+"product where `cid` in (select id from "+shopConfig.TablePre+"category where tid = ?) and type = 1 limit ?", cat_id, category_recommend_size); err != nil {
		executeResult["status"] = 0
		executeResult["msg"] = "数据查询异常" + err.Error()
		this.Data["json"] = executeResult
		this.ServeJSON()
		return
	}

	if limit, err = this.GetInt("limit"); err != nil {
		limit = 10
	}

	product = nil
	if err = Db.Select(&product, "select id,name,photo_little,price,price_preferential,saledcount from "+shopConfig.TablePre+"product where cid in (select id from "+shopConfig.TablePre+"category where tid = ?) order by saledcount desc limit ?", cat_id, category_recommend_hot_size); err != nil {
		executeResult["status"] = 0
		executeResult["msg"] = "数据查询异常" + err.Error()
		this.Data["json"] = executeResult
		this.ServeJSON()
		return
	}

	executeResult["status"] = 1
	executeResult["msg"] = "数据获取成功"
	executeResult["category_lists"] = categoryList
	executeResult["recommend_photo_lists"] = photoList
	executeResult["recommend_product_lists"] = product
	this.Data["json"] = executeResult
	this.ServeJSON()
}

func (this *ShopController) ListProductAction() {
	executeResult = make(map[string]interface{})
	if cat_id, err = this.GetInt("cat_id"); err != nil {
		executeResult["status"] = 0
		executeResult["msg"] = "参数异常" + err.Error()
		this.Data["json"] = executeResult
		this.ServeJSON()
	}

	if limit, err = this.GetInt("limit"); err != nil {
		limit = listproduct_size
	}

	if page, err = this.GetInt("p"); err != nil {
		page = 1
	}

	pagesize = (page - 1) * limit

	product = nil
	if err = Db.Select(&product, "select id,name,photo_little,price,price_preferential,saledcount,intro from "+shopConfig.TablePre+"product where cid =? order by saledcount desc limit ?,?", cat_id, pagesize, limit); err != nil {
		executeResult["status"] = 0
		executeResult["msg"] = "数据查询异常" + err.Error()
		this.Data["json"] = executeResult
		this.ServeJSON()
		return
	}

	executeResult["status"] = 1
	executeResult["msg"] = "数据获取成功"
	executeResult["data"] = product
	this.Data["json"] = executeResult
	this.ServeJSON()
}



//我的浏览足迹
func (this *ShopController) ProductViewAction() {
	//如果我们要删除map的所有的key,没有专门的方法一次删除，可以遍历key来删除或者 map=make(...),make一个新的，让原来的成为垃圾被gc回收
	executeResult = make(map[string]interface{})
	if page, err = this.GetInt("p" ); err != nil {
		//如果不传递，则默认为第一页
		page = 1
	}

	////带分页时候的记录数
	limit = (page - 1) * pagesize
	if uid,err = this.GetInt("uid" );err != nil {
		uid = 0
	}
	productView = nil
	if err =  Db.Select(&productView, "select id,pid,uid,(select name from `"+shopConfig.TablePre+"product`  where id = pid) as pname,(select photo_little from `"+shopConfig.TablePre+"product` where id = pid) as photo_little,num from `"+shopConfig.TablePre+"product_view` where uid=? order by id desc limit ?,?", uid, limit, pagesize); err != nil {

		executeResult["status"] = 0
		executeResult["msg"] = "数据异常" + err.Error() //实际业务中，不要暴露具体业务  //msg = "数据异常" + err.Error()
		this.Data["json"] = executeResult
		this.ServeJSON()
		return
	}

	executeResult["status"] = 1
	executeResult["page"] = page
	executeResult["msg"] = "数据获取成功"
	executeResult["data"] = productView
	this.Data["json"] = executeResult
	this.ServeJSON()
}

func (this *ShopController) ProductDetailAction() {
	executeResult = make(map[string]interface{})

	if pro_id, err = this.GetInt("pro_id"); err != nil {
		executeResult["status"] = 0
		executeResult["msg"] = "参数异常" + err.Error()
		this.Data["json"] = executeResult
		this.ServeJSON()
	}

	if uid, err = this.GetInt("uid"); err != nil {
		executeResult["status"] = 0
		executeResult["msg"] = "用户异常" + err.Error()
		this.Data["json"] = executeResult
		this.ServeJSON()
	}

	product = nil
	if err = Db.Select(&product, "select id,name,intro,cid,photo_little,photo_lists,price,price_preferential,saledcount,content,num from "+shopConfig.TablePre+"product where id =? and is_down = 0 and del = 0", pro_id); err != nil {
		executeResult["status"] = 0
		executeResult["msg"] = "数据异常" + err.Error()
		this.Data["json"] = executeResult
		this.ServeJSON()
	}

	collection = nil
	Db.Select(&collection, "select pid,uid from "+shopConfig.TablePre+"product_collection where uid = ? and pid = ?", uid, pro_id)

	photo_lists := strings.Split(product[0].PhotoLists, ",")


	//商品详情页 浏览足迹，实际业务过程当中，可以按天或者按周或者其他时间周期来处理，比如同一个用户对同一个商品一天内的浏览记录都记为一次，或者累加，浏览次数越多，说明用户对该商品的关注度越高，这也是为大数据部门做精准营销服务的。我们这里简单点处理，如果用户的浏览记录存在，我们直接让浏览次数在原有的基础上加1
	productView = nil
	err = Db.Select(&productView, "select `id`,`pid`,`num`,`uid` from `"+shopConfig.TablePre+"product_view` where `uid` = ? and `pid` = ?", uid, pro_id)

	if len(productView) > 0 {
		if _, err := Db.Exec("update `"+shopConfig.TablePre+"product_view` set num=num +1 where `uid` = ? and `pid` = ?",  uid, pro_id); err != nil {
			status = 0
			msg = "操作异常,请稍候再试" //不要将上面具体的业务信息打印出来，而是给到一个宽范的描述
		} else {
			status = 1
			msg = "添加成功"
		}
	} else {
		handle, err := Db.Exec("insert into `"+shopConfig.TablePre+"product_view`(`pid`, `uid`, `num`,`addtime`)values(?,?,?,?)", pro_id, uid, 1, time.Now().Unix())
		if err != nil {
			status = 0
			//msg = err.Error()
			msg = "操作异常,请稍候再试" //不使用将上面具体的业务信息打印出来，而是给到一个宽范的描述
		} else {
			_, err := handle.LastInsertId()
			if err != nil {
				status = 0
				//msg = err.Error()
				msg = "操作异常,请稍候再试" //不使用将上面具体的业务信息打印出来，而是给到一个宽范的描述
			} else {
				status = 1
				msg = "添加成功"
			}

		}
	}
	//是否增加浏览记录成功
	executeResult["addView"] = status


	executeResult["status"] = 1
	executeResult["msg"] = "数据获取成功"
	executeResult["product_detail"] = product
	executeResult["product_collection"] = collection
	executeResult["product_photo_lists"] = photo_lists
	this.Data["json"] = executeResult
	this.ServeJSON()
}

//个人中心，订单详情
func  (this *ShopController) UcenterOrderDetailAction() {
	//如果我们要删除map的所有的key,没有专门的方法一次删除，可以遍历key来删除或者 map=make(...),make一个新的，让原来的成为垃圾被gc回收
	executeResult = make(map[string]interface{})

	if id, err = this.GetInt64("order_id"); err != nil {
		executeResult["status"] = 0
		executeResult["msg"] = "订单异常"

		this.Data["json"] = executeResult
		this.ServeJSON()
		return
	}

	order = nil
	if err = Db.Select(&order, "select `id`,`order_sn`,`trade_no`,`status`,`price`,`price_real_pay`,`product_num`,`receiver`,`address_detail`,`addtime`,`remark` from `"+shopConfig.TablePre+"order` where `id` = ?", id); err != nil {
		executeResult["status"] = 0
		executeResult["msg"] = "订单异常"

		this.Data["json"] = executeResult
		this.ServeJSON()
		return
	}

	orderProduct = nil
	if err = Db.Select(&orderProduct, "select `id`,`pid`,`order_id`,`name`,`price`,`photo_little`,`num` from `"+shopConfig.TablePre+"order_product` where `order_id` = ?", id);err != nil {
		executeResult["status"] = 0
		executeResult["msg"] = "订单详情异常"

		this.Data["json"] = executeResult
		this.ServeJSON()
		return
	}

	executeResult["order"] = order
	executeResult["orderDetail"] = orderProduct
	executeResult["status"] = 1

	this.Data["json"] = executeResult
	this.ServeJSON()
}

//订单-取消订单
func  (this *ShopController) CancleOrderAction() {
	//如果我们要删除map的所有的key,没有专门的方法一次删除，可以遍历key来删除或者 map=make(...),make一个新的，让原来的成为垃圾被gc回收
	executeResult = make(map[string]interface{})

	if id, err = this.GetInt64("order_id"); err != nil {
		executeResult["status"] = 0
		executeResult["msg"] = "订单异常"

		this.Data["json"] = executeResult
		this.ServeJSON()
		return
	}

	//更新当前商品的已售数量和库存数量
	if _, err = Db.Exec("update `"+shopConfig.TablePre+"order` set del = ? where `id` = ?", 1, id); err != nil {
		executeResult["status"] = 0
		executeResult["msg"] = "取消失败"

		this.Data["json"] = executeResult
		this.ServeJSON()
		return
	}

	executeResult["status"] = 1
	executeResult["msg"] = "取消成功"

	this.Data["json"] = executeResult

	this.ServeJSON()
}

//个人中心-收货地址列表
func  (this *ShopController) MyAddressAction() {
	//如果我们要删除map的所有的key,没有专门的方法一次删除，可以遍历key来删除或者 map=make(...),make一个新的，让原来的成为垃圾被gc回收
	executeResult = make(map[string]interface{})

	//用户id
	uid, err = this.GetInt("uid")
	if err != nil {
		status = 0
		msg = "用户未登陆"

		executeResult["status"] = status
		executeResult["msg"] = msg

		this.Data["json"] = executeResult
		this.ServeJSON()
		return
	}

	address = nil
	if err = Db.Select(&address, "select `id`,`uid`,`name`,`tel`,`address_detail`,`is_default`  from `"+shopConfig.TablePre+"address` where `uid` = ?", uid); len(address) < 0 || err != nil {

		//executeResult["status"] = 0
		//executeResult["msg"] = "无收货地址"
		//
		//tools.ServeJSON(executeResult,w)
		//return
	}

	executeResult["status"] = 1
	executeResult["msg"] = "ok"
	executeResult["data"] = address

	this.Data["json"] = executeResult
	this.ServeJSON()
}

//个人中心-设置默认收货地址
func (this *ShopController) SetDefaultAddressAction() {
	//如果我们要删除map的所有的key,没有专门的方法一次删除，可以遍历key来删除或者 map=make(...),make一个新的，让原来的成为垃圾被gc回收
	executeResult = make(map[string]interface{})

	//地址记录的主键id
	if id, err = this.GetInt64("adid"); err != nil {
		executeResult["status"] = 0
		executeResult["msg"] = "地址有误"
		this.Data["json"] = executeResult
		this.ServeJSON()
		return
	}

	if uid, err = this.GetInt("uid"); err != nil {
		executeResult["status"] = 0
		executeResult["msg"] = "用户异常"

		this.Data["json"] = executeResult
		this.ServeJSON()
		return
	}

	//设置默认收货地址,1111111111先取消当前用户的所有收货地址的默认为0,这里比较粗暴
	if _, err = Db.Exec("update `"+shopConfig.TablePre+"address` set is_default= ? where `uid` = ?", 0, uid); err != nil {
		executeResult["status"] = 0
		executeResult["msg"] = "设置失败"

		this.Data["json"] = executeResult
		this.ServeJSON()
		return
	}

	//设置默认收货地址,22222222222再设置当前用户的指定的收货地址id为默认
	if _, err = Db.Exec("update `"+shopConfig.TablePre+"address` set is_default= ? where `id` = ? and `uid` = ?", 1, id, uid); err != nil {
		executeResult["status"] = 0
		executeResult["msg"] = "设置失败"

		this.Data["json"] = executeResult
		this.ServeJSON()
		return
	}

	executeResult["status"] = 1
	executeResult["msg"] = "设置成功"

	this.Data["json"] = executeResult
	this.ServeJSON()
}

//个人中心-删除指定收货地址,这里是真正的删除，因为没有必要留着过多无用的地址
func (this *ShopController) DelAddressAction() {
	//如果我们要删除map的所有的key,没有专门的方法一次删除，可以遍历key来删除或者 map=make(...),make一个新的，让原来的成为垃圾被gc回收
	executeResult = make(map[string]interface{})

	//地址记录的主键id
	if id, err = this.GetInt64("adid"); err != nil {
		executeResult["status"] = 0
		executeResult["msg"] = "地址有误"

		this.Data["json"] = executeResult
		this.ServeJSON()
		return
	}

	if uid, err = this.GetInt("uid"); err != nil {
		executeResult["status"] = 0
		executeResult["msg"] = "用户异常"

		this.Data["json"] = executeResult
		this.ServeJSON()
		return
	}

	//这里是真正的删除，因为没有必要留着过多无用的地址
	//删除指定的收货地址												通过id即可确定唯一，这里我们还是把Uid带上
	if _, err = Db.Exec("delete from `"+shopConfig.TablePre+"address` where `id` = ? and uid = ?", id, uid); err != nil {
		executeResult["status"] = 0
		executeResult["msg"] = "删除异常"

		this.Data["json"] = executeResult
		this.ServeJSON()
		return
	}

	executeResult["status"] = 1
	executeResult["msg"] = "删除成功"

	this.Data["json"] = executeResult
	this.ServeJSON()
}

//个人中心-收货地址-添加收货地址-获取省市区
func (this *ShopController) MapAction() {
	//如果我们要删除map的所有的key,没有专门的方法一次删除，可以遍历key来删除或者 map=make(...),make一个新的，让原来的成为垃圾被gc回收
	executeResult = make(map[string]interface{})

	//省市区id
	tid, err := this.GetInt("tid" )
	if err != nil {
		tid = 0 //一级
	}

	maps = nil
	if err = Db.Select(&maps, "select `id`,`tid`,`name`,`code` from `"+shopConfig.TablePre+"map` where `tid` = ?", tid); len(maps) < 0 || err != nil {
		executeResult["status"] = 0
		executeResult["msg"] = "数据异常"

		this.Data["json"] = executeResult
		this.ServeJSON()
		return
	}

	executeResult["status"] = 1
	executeResult["msg"] = "ok"
	executeResult["data"] = maps
	this.Data["json"] = executeResult
	this.ServeJSON()
}

//个人中心-收货地址-添加收货地址-入库
func (this *ShopController) AddAddressAction() {

	receiver := this.GetString("receiver")

	if receiver == "" {
		executeResult["status"] = 0
		executeResult["msg"] = "请输入姓名"

		this.Data["json"] = executeResult
		this.ServeJSON()
		return
	}

	if uid, err = this.GetInt("user_id"); err != nil {
		executeResult["status"] = 0
		executeResult["msg"] = "用户异常"

		this.Data["json"] = executeResult
		this.ServeJSON()
		return
	}

	//这里的联系方式做为字符串处理，因为有可能是坐机，坐机中含有-或者,等
	tel := this.GetString("tel")
	if tel == "" {
		executeResult["status"] = 0
		executeResult["msg"] = "联系方式"

		this.Data["json"] = executeResult
		this.ServeJSON()
		return
	}

	//省级
	province := this.GetString("province")
	if province == "" {
		executeResult["status"] = 0
		executeResult["msg"] = "省级异常"

		this.Data["json"] = executeResult
		this.ServeJSON()
		return
	}

	//市级
	city := this.GetString("city" )
	if city == "" {
		executeResult["status"] = 0
		executeResult["msg"] = "市级异常"

		this.Data["json"] = executeResult
		this.ServeJSON()
		return
	}

	//区级
	countryard := this.GetString("countryard" )
	if countryard == "" {
		executeResult["status"] = 0
		executeResult["msg"] = "区级异常"

		this.Data["json"] = executeResult
		this.ServeJSON()
		return
	}

	//详细地址
	address := this.GetString("address")
	if address == "" {
		executeResult["status"] = 0
		executeResult["msg"] = "详细地址异常"

		this.Data["json"] = executeResult
		this.ServeJSON()
		return
	}

	address_detail := province + " " + city + " " + countryard + " " + address

	//邮编
	code, err := this.GetInt("code")
	if err != nil {
		code = 0
	}

	//province,city,countryard这几个int类型的字段没有必要存储，因为用户无法修改自己的地址，同学可以根据自身业务的需要进行扩充
	handle, err := Db.Exec("insert into `"+shopConfig.TablePre+"address`(`uid`,`name`,`tel`,`address`,`address_detail`,`code`)values(?,?,?,?,?,?)", uid, receiver, tel, address, address_detail, code)
	if err != nil {
		executeResult["status"] = 0
		executeResult["msg"] = "操作失败"

		this.Data["json"] = executeResult
		this.ServeJSON()
		return
	}

	executeResult["insertid"], err = handle.LastInsertId()

	this.Data["json"] = executeResult
	this.ServeJSON()
}

func (this *ShopController) GetMyCollectAction() {
	//如果我们要删除map的所有的key,没有专门的方法一次删除，可以遍历key来删除或者 map=make(...),make一个新的，让原来的成为垃圾被gc回收
	executeResult = make(map[string]interface{})

	//uid
	if uid, err = this.GetInt("uid"); err != nil {
		executeResult["status"] = 0
		executeResult["msg"] = "用户异常" + err.Error() //实际业务中，不要暴露具体业务  //msg = "数据异常" + err.Error()

		this.Data["json"] = executeResult
		this.ServeJSON()
		return
	}

	myCollect = nil
	if err = Db.Select(&myCollect, "select c.pid,c.uid,p.photo_little,p.name,p.price_preferential,p.intro from `"+shopConfig.TablePre+"product_collection` c join  `"+shopConfig.TablePre+"product` p on p.id = c.pid and c.uid=?", uid); err != nil {
		executeResult["status"] = 0
		executeResult["msg"] = "数据异常" + err.Error() //实际业务中，不要暴露具体业务  //msg = "数据异常" + err.Error()

		this.Data["json"] = executeResult
		this.ServeJSON()
		return
	}

	executeResult["status"] = 1
	executeResult["msg"] = "数据获取成功"
	executeResult["data"] = myCollect
	this.Data["json"] = executeResult
	this.ServeJSON()
}

//个人中心-我的收藏---取消收藏,此方法不需要，因为在CollectAction当中已经存在
func (this *ShopController) CancleCollectAction() {
	//如果我们要删除map的所有的key,没有专门的方法一次删除，可以遍历key来删除或者 map=make(...),make一个新的，让原来的成为垃圾被gc回收
	executeResult = make(map[string]interface{})

	//商品id
	if pro_id, err = this.GetInt("pid" ); err != nil {
		executeResult["status"] = 0
		executeResult["msg"] = "请选择收藏的商品" //实际业务中，不要暴露具体业务

		this.Data["json"] = executeResult
		this.ServeJSON()
		return
	}

	//用户id
	if uid, err = this.GetInt("uid" ); err != nil {
		executeResult["status"] = 0
		executeResult["msg"] = "用户异常" //实际业务中，不要暴露具体业务

		this.Data["json"] = executeResult
		this.ServeJSON()
		return
	}

	//当前登陆用户是否收藏该商品
	err = Db.Select(&collection, "select `pid`,`uid`,`status` from `"+shopConfig.TablePre+"product_collection` where `uid` = ? and `pid` = ? and `status`=?", uid, pro_id, 1)

	var msg string
	var tip string
	var status int
	if len(collection) > 0 {
		_, err := Db.Exec("delete from `"+shopConfig.TablePre+"product_collection` where `uid` = ? and `pid` = ? and `status`=?", uid, pro_id, 1)
		if err != nil {
			msg = err.Error()
			//tip = "收藏"
			status = 0
		} else {
			msg = "取消收藏成功"
			tip = "收藏"
			status = 1
		}
	}

	executeResult["msg"] = msg
	executeResult["tip"] = tip
	executeResult["status"] = status

	this.Data["json"] = executeResult
	this.ServeJSON()
}

//个人中心，优惠券列表
func (this *ShopController) CouponListAction() {
	//如果我们要删除map的所有的key,没有专门的方法一次删除，可以遍历key来删除或者 map=make(...),make一个新的，让原来的成为垃圾被gc回收
	executeResult = make(map[string]interface{})

	//仅登陆的用户允许查看平台的优惠券
	if uid, err = this.GetInt("uid"); err != nil || uid == 0 {
		executeResult["status"] = 0
		executeResult["msg"] = "用户异常"

		this.Data["json"] = executeResult
		this.ServeJSON()
		return
	}

	if page, err = this.GetInt("p"); err != nil || page == 0 {
		//页数有误，强制第一页
		page = 1
	}

	//带分页时候的记录数
	limit = (page - 1) * pagesize
	del := 0 //订单状态 0正常 1删除，这里不要写si,待需要变更时，可以灵活配置,存储到配置文件，功能完成再添加去

	err = Db.Select(&totalRow, "select count(1) as total from `"+shopConfig.TablePre+"coupon` where `del`=?", del)
	//err = Db.Select(&coupon, "select id,shop_id,title,much_money,amount,from_unixtime(start_time,'%Y-%m-%d %H:%i:%S') as start_time,from_unixtime(end_time,'%Y-%m-%d %H:%i:%S') as end_time,point,count,get_coupon_num,del,from_unixtime(addtime,'%Y-%m-%d %H:%i:%S') as addtime,proid from `"+shopConfig.TablePre+"coupon` where `del` = ? order by id desc limit ?,?",  del, limit, pagesize)
	coupon = nil
	err = Db.Select(&coupon, "select id,shop_id,title,much_money,amount,from_unixtime(start_time,'%Y-%m-%d') as start_time,from_unixtime(end_time,'%Y-%m-%d') as end_time,point,count,get_coupon_num,del,from_unixtime(addtime,'%Y-%m-%d') as addtime,proid from `"+shopConfig.TablePre+"coupon` where `del` = ? order by id desc limit ?,?", del, limit, pagesize)

	if err != nil {
		executeResult["status"] = 0
		executeResult["msg"] = "优惠券异常" + err.Error()

		this.Data["json"] = executeResult
		this.ServeJSON()
		return
	}
	executeResult["status"] = 1
	executeResult["msg"] = "ok"
	executeResult["data"] = coupon

	this.Data["json"] = executeResult
	this.ServeJSON()
}

//个人中心，优惠券领取
func (this *ShopController) GetCouponAction() {
	//如果我们要删除map的所有的key,没有专门的方法一次删除，可以遍历key来删除或者 map=make(...),make一个新的，让原来的成为垃圾被gc回收
	executeResult = make(map[string]interface{})

	//仅登陆的用户允许领取平台的优惠券
	if uid, err = this.GetInt("uid" ); err != nil || uid == 0 {
		executeResult["status"] = 0
		executeResult["msg"] = "用户异常"

		this.Data["json"] = executeResult
		this.ServeJSON()
		return
	}
	//优惠券id
	if coupid, err = this.GetInt64("couponId"); err != nil || uid == 0 {
		executeResult["status"] = 0
		executeResult["msg"] = "优惠券异常"

		this.Data["json"] = executeResult
		this.ServeJSON()
		return
	}
	//积分
	if point, err = this.GetInt64("point" ); err != nil {
		//可以为0，不做任何处理
	}

	totalRow = nil
	err = Db.Select(&totalRow, "select count(1) as total from `"+shopConfig.TablePre+"user_coupon` where `vid`=? and `uid` = ?", coupid, uid)

	if totalRow[0].Total > 0 {
		executeResult["status"] = 0
		executeResult["msg"] = "你已领取过此优惠券"

		this.Data["json"] = executeResult
		this.ServeJSON()
		return
	}

	user = nil

	if err = Db.Select(&user, "select `id`,`integral` from `"+shopConfig.TablePre+"user` where `id`=?", uid); err != nil {
		executeResult["status"] = 0
		executeResult["msg"] = "用户异常" + err.Error()

		this.Data["json"] = executeResult
		this.ServeJSON()
		return
	}

	if user[0].Integral < point {
		executeResult["status"] = 0
		executeResult["msg"] = "积分不足"
		this.Data["json"] = executeResult
		this.ServeJSON()
		return
	}

	coupon = nil
	if err = Db.Select(&coupon, "select id,shop_id,title,much_money,amount,start_time, end_time,point,count,get_coupon_num,del,addtime,proid from `"+shopConfig.TablePre+"coupon` where `del` = ? and `id` = ?", coupon_del, coupid); err != nil {
		executeResult["status"] = 0
		executeResult["msg"] = "优惠券异常" + err.Error()
		this.Data["json"] = executeResult
		this.ServeJSON()
		return
	}


	//判断优惠券的状态是否过期，是否使用等，这里我将思路抛给大家来处理

	if coupon[0].Get_coupon_num >= coupon[0].Count {
		executeResult["status"] = 0
		executeResult["msg"] = "优惠券已领完，下次继续"
		this.Data["json"] = executeResult
		this.ServeJSON()
		return
	}

	handle, err_insert := Db.Exec("insert into `"+shopConfig.TablePre+"user_coupon`(`uid`, `vid`, `addtime`,`status`)values(?,?,?,?)", uid, coupid, time.Now().Unix(), coupon_status)

	_, err_update_coupon := Db.Exec("update `"+shopConfig.TablePre+"coupon` set get_coupon_num=get_coupon_num+1 where `id` = ?", coupid)

	_, err_update_point := Db.Exec("update `"+shopConfig.TablePre+"user` set integral=integral-? where `id` = ?", point, uid)

	if err_insert != nil && err_update_coupon != nil && err_update_point != nil {
		executeResult["status"] = 0
		executeResult["msg"] = "领取异常" + err.Error()

	} else {
		id, err := handle.LastInsertId()
		if err != nil {
			executeResult["status"] = 0
			executeResult["msg"] = "领取异常" + err.Error()
		} else {
			executeResult["status"] = 1
			executeResult["msg"] = "优惠券领取成功"
			executeResult["insertid"] = id
		}
	}

	this.Data["json"] = executeResult
	this.ServeJSON()
}

//个人中心，我的权益---我领取的优惠券
func (this *ShopController) GetMyCouponAction() {
	//如果我们要删除map的所有的key,没有专门的方法一次删除，可以遍历key来删除或者 map=make(...),make一个新的，让原来的成为垃圾被gc回收
	executeResult = make(map[string]interface{})

	//仅登陆的用户允许领取平台的优惠券
	if uid, err = this.GetInt("uid" ); err != nil || uid == 0 {
		executeResult["status"] = 0
		executeResult["msg"] = "用户异常"
		this.Data["json"] = executeResult
		this.ServeJSON()
		return
	}

	myUserCoupon = nil
	if err = Db.Select(&myUserCoupon, "select  c.title,c.much_money,c.point,c.amount,uc.status,c.proid,from_unixtime(start_time,'%Y-%m-%d') as start_time,from_unixtime(end_time,'%Y-%m-%d') as end_time  from `"+shopConfig.TablePre+"coupon` c join `"+shopConfig.TablePre+"user_coupon` uc on c.id = uc.vid and uc.uid = ?", uid); err != nil {
		executeResult["status"] = 0
		executeResult["msg"] = "获取优惠券异常"
		this.Data["json"] = executeResult
		this.ServeJSON()
		return
	}

	executeResult["status"] = 1
	executeResult["msg"] = "获取数据成功"
	executeResult["data"] = myUserCoupon

	this.Data["json"] = executeResult
	this.ServeJSON()
}

//分类页----轮播图与下方的热销商品推荐
func (this *ShopController) SwiperHotProductAction() {
	//如果我们要删除map的所有的key,没有专门的方法一次删除，可以遍历key来删除或者 map=make(...),make一个新的，让原来的成为垃圾被gc回收
	executeResult = make(map[string]interface{})

	//是否推荐 type=1
	if type_id, err = this.GetInt("type"); err != nil {
		type_id = 1
	}

	//分类id,这里获取的是一级分类
	if cat_id, err = this.GetInt("cat_id"); err != nil {
		executeResult["status"] = 0
		executeResult["msg"] = "请输入一级分类" //实际业务中，不要暴露具体业务
		this.Data["json"] = executeResult
		this.ServeJSON()
		return
	}

	//不带分页时候的记录数
	if limit, err = this.GetInt("limit"); err != nil {
		limit = limits
	}

	photoLists = nil
	if err = Db.Select(&photoLists, "select id,name,photo_big from `"+shopConfig.TablePre+"product` where `type`=? and cid in (SELECT id from `"+shopConfig.TablePre+"category` where tid=?) order by id desc limit ?", type_id, cat_id, limit); err != nil {
		executeResult["status"] = 0
		executeResult["msg"] = "数据异常" //实际业务中，不要暴露具体业务
		this.Data["json"] = executeResult
		this.ServeJSON()
		return
	}

	product = nil
	err = Db.Select(&product, "select id,intro,cid,name,photo_little,photo_lists,price,price_preferential,saledcount,brand_id from `"+shopConfig.TablePre+"product` where `type`=? and cid in  (SELECT id from `"+shopConfig.TablePre+"category` where tid=?) order by id desc limit ?", type_id, cat_id, limit)

	executeResult["recommend_photo_lists"] = photoLists
	executeResult["recommend_product_lists"] = product

	//如果我们要删除map的所有的key,没有专门的方法一次删除，可以遍历key来删除或者 map=make(...),make一个新的，让原来的成为垃圾被gc回收

	this.Data["json"] = executeResult
	this.ServeJSON()

	//logs.SetLogger(logs.AdapterFile, string(configStr))

	//根据logs.LevelDebug的级别，将不同级别的日志写到文件 中
	//logs.Debug("strs-Debug %s", executeResult)
	//logs.Trace("strs-Trace %s", strs)
	//logs.Warn("strs-Warn %s", strs)
}


func (this *ShopController) ProductToCartsAction() {
	executeResult = make(map[string]interface{})

	if pro_id, err = this.GetInt("pro_id"); err != nil {
		executeResult["status"] = 0
		executeResult["msg"] = "参数异常" + err.Error()
		this.Data["json"] = executeResult
		this.ServeJSON()
		return
	}

	if uid, err = this.GetInt("uid"); err != nil {
		executeResult["status"] = 0
		executeResult["msg"] = "用户异常" + err.Error()
		this.Data["json"] = executeResult
		this.ServeJSON()
		return
	}

	if num, err = this.GetInt("num"); err != nil {
		executeResult["status"] = 0
		executeResult["msg"] = "购物车数量异常"
		this.Data["json"] = executeResult
		this.ServeJSON()
		return
	}

	product = nil
	if err = Db.Select(&product, "select id,num,price_preferential from "+shopConfig.TablePre+"product where id = ? and is_down=0 and del=0", pro_id); err != nil || len(product) <= 0 {
		executeResult["status"] = 0
		executeResult["msg"] = "商品不存在或下架"
		this.Data["json"] = executeResult
		this.ServeJSON()
		return
	}

	if product[0].Num < num {
		executeResult["status"] = 0
		executeResult["msg"] = "商品库存不足"
		this.Data["json"] = executeResult
		this.ServeJSON()
		return
	}

	carts = nil
	if err = Db.Select(&carts, "select id,pid,price,num,uid from "+shopConfig.TablePre+"cart where uid = ? and pid = ?", uid, pro_id); err != nil {
		executeResult["status"] = 0
		executeResult["msg"] = "数据库操作异常" + err.Error()
		this.Data["json"] = executeResult
		this.ServeJSON()
		return
	}

	var newCartNum = 0
	if len(carts) > 0 {
		//更新购物车中商品的数量
		newCartNum = carts[0].Num + num
		if newCartNum > product[0].Num {
			executeResult["status"] = 0
			executeResult["msg"] = "商品库存不足"
			this.Data["json"] = executeResult
			this.ServeJSON()
			return
		}

		cart_id	 = int64(carts[0].Id)

		if _, err = Db.Exec("update "+shopConfig.TablePre+"cart set num = ? where uid = ? and pid = ?", newCartNum, uid, pro_id); err != nil {
			executeResult["status"] = 0
			executeResult["msg"] = "操作异常"
			this.Data["json"] = executeResult
			this.ServeJSON()
			return
		}
	} else {
		newCartNum = num
		var handler sql.Result
		if handler, err = Db.Exec("insert "+shopConfig.TablePre+"cart(pid,price,num,addtime,uid) values(?,?,?,?,?)", pro_id, product[0].PricePreferential, newCartNum, time.Now().Unix(), uid); err != nil {
			executeResult["status"] = 0
			executeResult["msg"] = "操作异常"
			this.Data["json"] = executeResult
			this.ServeJSON()
			return
		}

		cart_id	,_ = handler.LastInsertId()
	}

	executeResult["status"] = 1
	executeResult["msg"] = "添加购物车成功"
	executeResult["cart_id"] = cart_id
	this.Data["json"] = executeResult
	this.ServeJSON()
	return
}

func (this *ShopController) CollectionAction() {
	executeResult = make(map[string]interface{})

	if pro_id, err = this.GetInt("pro_id"); err != nil {
		executeResult["status"] = 0
		executeResult["msg"] = "参数异常" + err.Error()
		this.Data["json"] = executeResult
		this.ServeJSON()
		return
	}

	if uid, err = this.GetInt("uid"); err != nil {
		executeResult["status"] = 0
		executeResult["msg"] = "用户异常" + err.Error()
		this.Data["json"] = executeResult
		this.ServeJSON()
		return
	}
	collection = nil
	if err = Db.Select(&collection, "select pid,uid from "+shopConfig.TablePre+"product_collection where uid = ? and pid = ?", uid, pro_id); err != nil {
		executeResult["status"] = 0
		executeResult["msg"] = "数据库操作异常" + err.Error()
		this.Data["json"] = executeResult
		this.ServeJSON()
		return
	}

	if len(collection) > 0 {
		if _, err = Db.Exec("delete from "+shopConfig.TablePre+"product_collection where uid = ? and pid = ?", uid, pro_id); err != nil {
			executeResult["status"] = 0
			executeResult["msg"] = "数据库操作异常" + err.Error()
			this.Data["json"] = executeResult
			this.ServeJSON()
			return
		} else {
			executeResult["status"] = 1
			executeResult["msg"] = "取消收藏成功"
			executeResult["tip"] = "加入收藏"
			this.Data["json"] = executeResult
			this.ServeJSON()
			return
		}

	} else {
		if _, err = Db.Exec("insert into "+shopConfig.TablePre+"product_collection (uid,pid,addtime) values(?,?,?)", uid, pro_id, time.Now().Unix()); err != nil {
			executeResult["status"] = 0
			executeResult["msg"] = "数据库操作异常" + err.Error()
			this.Data["json"] = executeResult
			this.ServeJSON()
			return
		}

		executeResult["status"] = 1
		executeResult["msg"] = "收藏成功"
		executeResult["tip"] = "取消收藏"
		this.Data["json"] = executeResult
		this.ServeJSON()
		return
	}

}

func (this *ShopController) MyCartListAction() {
	executeResult = make(map[string]interface{})

	if uid, err = this.GetInt("uid"); err != nil {
		executeResult["status"] = 0
		executeResult["msg"] = "用户异常" + err.Error()
		this.Data["json"] = executeResult
		this.ServeJSON()
		return
	}

	carts = nil
	if err = Db.Select(&carts, "select c.id,c.uid,c.pid,c.price,c.num,p.photo_little,p.name from "+shopConfig.TablePre+"cart c join imooc_product p on c.pid = p.id and c.uid = ?", uid); err != nil {
		executeResult["status"] = 0
		executeResult["msg"] = "数据库操作异常" + err.Error()
		this.Data["json"] = executeResult
		this.ServeJSON()
		return
	}

	product = nil
	err = Db.Select(&product, "select id,intro,cid,name,photo_little,photo_lists,price,price_preferential,saledcount from `"+shopConfig.TablePre+"product` where `type`=? order by id desc limit ?", 1, category_recommend_hot_size)

	//logs.Debug("%s\n",cartlist)

	executeResult["productlist"] = product
	executeResult["cartlist"] = nil
	if len(product) > 0 {
		executeResult["productlist"] = product
	}
	if len(carts) > 0 {
		executeResult["cartlist"] = carts
	}

	executeResult["status"] = 1
	executeResult["msg"] = "数据获取成功"
	this.Data["json"] = executeResult
	this.ServeJSON()
	return

}

func (this *ShopController) CartsToBuyAction() {

	executeResult = make(map[string]interface{})

	if uid, err = this.GetInt("uid"); err != nil {
		executeResult["status"] = 0
		executeResult["msg"] = "用户异常" + err.Error()
		this.Data["json"] = executeResult
		this.ServeJSON()
		return
	}

	userCoupon = nil
	if err = Db.Select(&userCoupon, "select vid,status,uid from "+shopConfig.TablePre+"user_coupon where uid=? and status=1 order by addtime desc", uid); err != nil {
		executeResult["status"] = 0
		executeResult["msg"] = "用户优惠券异常" + err.Error()
		this.Data["json"] = executeResult
		this.ServeJSON()
		return
	}

	if len(userCoupon) > 0 {
		executeResult["couponDetail"] = nil
		currentTime := time.Now().Unix()
		couponDetail = nil
		for _, v := range userCoupon {
			//v.Vid—
			if err = Db.Select(&couponDetail, "select id,title,much_money,amount from "+shopConfig.TablePre+"coupon where id = ? and start_time <= ? and end_time>= ?", v.Vid, currentTime, currentTime); err != nil {
				executeResult["status"] = 0
				executeResult["msg"] = "用户优惠券异常" + err.Error()
				this.Data["json"] = executeResult
				this.ServeJSON()
				return
			}

			if len(couponDetail) > 0 {
				executeResult["couponDetail"] = couponDetail
			}
		}
	}
	//cart_id = 1,2,3,
	//cart_id[:len(cart_id)-1] //1,2,3
	cart_ids = this.GetString("cart_ids")
	cart_ids = cart_ids[:len(cart_ids)-1]
	cart_id := strings.Split(cart_ids, ",")

	productCarts = nil
	for _, v := range cart_id {
		if err = Db.Select(&productCarts, "select  p.num as pnum,p.name,p.photo_little,c.price,c.uid,c.num from "+shopConfig.TablePre+"cart c join "+shopConfig.TablePre+"product p on p.id = c.pid where c.uid = ? and c.id = ?", uid, v); err != nil {
			executeResult["status"] = 0
			executeResult["msg"] = "购物车异常" + err.Error()
			this.Data["json"] = executeResult
			this.ServeJSON()
			return
		}
	}
	price_preferential := 0.00 //商品总价，初始为0.00
	pro_num := 0               //购物车商品的数量
	if len(productCarts) > 0 {
		for _, v := range productCarts {
			price_preferential += float64(v.Num) * v.Price
			pro_num += v.Num
		}
	}

	address = nil
	if err = Db.Select(&address, "select id,uid,name,tel,address_detail,is_default from "+shopConfig.TablePre+"address where uid =? and is_default=1", uid); err != nil {
		executeResult["status"] = 0
		executeResult["msg"] = "收货地址异常" + err.Error()
		this.Data["json"] = executeResult
		this.ServeJSON()
		return
	}
	have_default_addr := false //没有默认收货地址
	if len(address) > 0 {
		have_default_addr = true
	}


	usercoupon = nil
	err = Db.Select(&usercoupon, "SELECT `vid` FROM `"+shopConfig.TablePre+"user_coupon` WHERE `uid` = ? AND `status` = 1 ORDER BY addtime desc", uid)
	if err != nil {
		status = 0
		msg = "用户的优惠券异常"
		executeResult["status"] = status
		executeResult["msg"] = msg

		this.Data["json"] = executeResult
		this.ServeJSON()
		return
	}

	couponDetail = nil
	for _, vid := range usercoupon {
		//优惠券适应的商品id，如果为all,则去除掉proid字段

		//这里通过where id=?即可唯一确定，我们人为添加limit 1
		//and `end_time` <= ?
		err = Db.Select(&couponDetail, "select id,title,much_money,amount from  `"+shopConfig.TablePre+"coupon` where ( id=? ) and `start_time` <= ? limit 1", vid.Vid, time.Now().Unix())

		if err != nil {
			status = 0
			msg = "用户的详细优惠券异常"
			executeResult["status"] = status
			executeResult["msg"] = msg

			this.Data["json"] = executeResult
			this.ServeJSON()
			return
		}
	}

	//优惠券
	executeResult["coupon"] = couponDetail
	executeResult["productData"] = productCarts

	executeResult["status"] = 1
	executeResult["msg"] = "数据获取成功"
	executeResult["have_default_addr"] = have_default_addr
	executeResult["address"] = address
	executeResult["total_price"] = price_preferential
	executeResult["productCarts"] = productCarts
	executeResult["pro_num"] = pro_num
	this.Data["json"] = executeResult
	this.ServeJSON()
}

//下单页---点击支付按钮，调取微信支付去支付，这里使用github
func (this *ShopController)OrderToPayAction()  {

	//如果我们要删除map的所有的key,没有专门的方法一次删除，可以遍历key来删除或者 map=make(...),make一个新的，让原来的成为垃圾被gc回收
	executeResult = make(map[string]interface{})
	//理论上在插入数据库表的时候也是需要做一次判断的,也就是说只传入当前购物车的信息，用户的信息，优惠券的信息，点击支付时进行业务的判断，(因为在插入的时候，别的用户也有可能在操作，所以需要判断
	//库存是否正常
	//购物车中的商品是否正常，为了防止恶意下单
	//优惠券是否有效，包括是否开始，是否结束，是否过期等
	//这里我们就简化了，这个过程和上面加入到预下单页的逻辑判断类似，这块业务逻辑大家参照上面预下单的逻辑来处理即可，由于这里的逻辑比较简单，我们呢就直接入库了，我呢将更多的思路留给各位同学来实现，发吧。

	//支付订单号
	order_sn := this.GetString("order_sn")
	order_id, err := this.GetInt("order_id")
	if uid, err = this.GetInt("uid"); err != nil {
		status = 0
		msg = "用户未登陆"

		executeResult["status"] = status
		executeResult["msg"] = msg
		this.Data["json"] = executeResult
		this.ServeJSON()
		return
	}

	order = nil
	del := 0 //订单状态 0正常 1删除，这里不要写si,待需要变更时，可以灵活配置,存储到配置文件，功能完成再添加去

	//当前订单的支付状态，1未付款  2已付款待发货 3已发货待收货 4收到货交易完成 5退款退货申请  6退款退货完成或者说订单关闭
	paystatus := 1

	err = Db.Select(&order, "select `id`,`order_sn`,`trade_no`,`status`,`price`,`price_real_pay`,`product_num` from `"+shopConfig.TablePre+"order` where `uid` = ? and `del` = ? and `status` = ? and `id`=?  and `order_sn` = ?", uid, del, paystatus, order_id, order_sn)

	currentUnix := time.Now().Unix()

	if err != nil {
		status = 0
		//msg = err.Error()
		//订单异常，包括订单是否存在，订单的状态是否正确（比如已经支付的订单，已经申请退款的订单），详细逻辑各位同学可以一一来展开
		msg = "订单异常" + err.Error()

		executeResult["status"] = status
		executeResult["msg"] = msg

		this.Data["json"] = executeResult
		this.ServeJSON()
		return
	}

	//err = Db.Select(&user, "select `id`,`openid` from `"+shopConfig.TablePre+"user` where `id`=?", uid)
	user = nil
	if err = Db.Select(&user, "select `id`,`openid` from `"+shopConfig.TablePre+"user` where `id`=? and openid != ''", uid); err != nil {
		status = 0
		msg = "用户异常"

		executeResult["status"] = status
		executeResult["msg"] = msg

		this.Data["json"] = executeResult
		this.ServeJSON()
		return
	}

	///////////////////////////////////////////////////////微信小程序支付start///////////////////////////////////////////////////////

	MiniPay.InitMiniPay(&MiniPay.MiniPayParams{
		AppID: "wx6cd93ed0194fae7f",
		MchID: "1483469312",
		Key:   "098F6BCD4621D373CADE4E832627B4F6",
		//PrivateKey: []byte{},
		//PublicKey: []byte{},
	})

	charge := new(MiniPay.PayArg)
	//charge.PayMethod = common.MINI_PROGRAM
	//以下为测试时的数据，具体改成实际获取的
	charge.MoneyFee = 1
	charge.Body = "copyrightat波哥"
	charge.TradeNum = order[0].Order_sn
	charge.CallbackURL = "https://miniprograme.onajax.com/Api/callback"


	charge.OpenID = user[0].Openid
	fdata, err := MiniPay.Order2Pay(charge)
	if err != nil {
		fmt.Println(err.Error())
	}

	if err != nil {
		fmt.Println(err)
	}
	fmt.Printf("%+v", fdata)

	executeResult["order_id"] = order_id
	executeResult["order_sn"] = currentUnix
	executeResult["msg"] = "微信下发成功"
	executeResult["status"] = 1
	executeResult["payinfo"] = fdata

	this.Data["json"] = executeResult
	this.ServeJSON()
}

func (this *ShopController) OrderToPrepayAction() {
	executeResult = make(map[string]interface{})

	if price, err = this.GetFloat("price"); err != nil {
		executeResult["status"] = 0
		executeResult["msg"] = "价格异常" + err.Error()
		this.Data["json"] = executeResult
		this.ServeJSON()
		return
	} //不使用优惠券之后的价格
	if minusPrice, err = this.GetFloat("minusPrice"); err != nil {
		executeResult["status"] = 0
		executeResult["msg"] = "价格异常" + err.Error()
		this.Data["json"] = executeResult
		this.ServeJSON()
		return
	} //使用优惠券之后的价格

	currentTime := time.Now().Unix()

	if uid, err = this.GetInt("uid"); err != nil {
		executeResult["status"] = 0
		executeResult["msg"] = "用户异常" + err.Error()
		this.Data["json"] = executeResult
		this.ServeJSON()
		return
	}

	couponid, err = this.GetInt("couponid")
	remark = this.GetString("remark")
	address_detail = this.GetString("address_detail")
	if address_detail == "" {
		executeResult["status"] = 0
		executeResult["msg"] = "请选择收货地址"
		this.Data["json"] = executeResult
		this.ServeJSON()
		return
	}
	tel = this.GetString("tel")
	concat = this.GetString("concat")
	if proNum, err = this.GetInt("pro_num"); err != nil {
		executeResult["status"] = 0
		executeResult["msg"] = "商品数量异常" + err.Error()
		this.Data["json"] = executeResult
		this.ServeJSON()
		return
	}

	//开启事务
	conn, err := Db.Begin()

	if err != nil {
		executeResult["status"] = 0
		executeResult["msg"] = "事务开启异常" + err.Error()
		this.Data["json"] = executeResult
		this.ServeJSON()
		return
	}
	//1.插入订单表
	handle, errs := Db.Exec("insert into "+shopConfig.TablePre+"order(uid,order_sn,price,amount,vid,price_real_pay,addtime,receiver,tel,address_detail,remark,product_num) values(?,?,?,?,?,?,?,?,?,?,?,?)", uid, currentTime, price, minusPrice, couponid, minusPrice, currentTime, concat, tel, address_detail, remark, proNum)

	if errs != nil {
		conn.Rollback()
		executeResult["status"] = 0
		executeResult["msg"] = "插入订单异常" + err.Error()
		this.Data["json"] = executeResult
		this.ServeJSON()
		return
	}

	orderid, err := handle.LastInsertId()

	cart_ids = this.GetString("cart_ids")
	cart_ids = cart_ids[:len(cart_ids)-1]
	cart_id := strings.Split(cart_ids, ",")

	productCarts = nil
	for _, v := range cart_id {
		if err = Db.Select(&productCarts, "select  p.num as pnum,p.name,p.photo_little,c.price,c.uid,c.pid,c.id,c.num from "+shopConfig.TablePre+"cart c join "+shopConfig.TablePre+"product p on p.id = c.pid where c.uid = ? and c.id = ?", uid, v); err != nil {
			executeResult["status"] = 0
			executeResult["msg"] = "购物车异常" + err.Error()
			this.Data["json"] = executeResult
			this.ServeJSON()
			return
		}
	}
	if len(productCarts) > 0 {
		isExecuteSuccess := true
		for _, v := range productCarts {
			//2.插入订单详情表
			_, err = Db.Exec("insert into "+shopConfig.TablePre+"order_product (pid,order_id,name,price,photo_little,addtime,num) values(?,?,?,?,?,?,?) ", v.Pid, orderid, v.Name, v.Price, v.PhotoLittle, currentTime, v.Num)
			if err != nil {
				isExecuteSuccess = false
				conn.Rollback()
				executeResult["status"] = 0
				executeResult["msg"] = "插入详情表异常" + err.Error()
				this.Data["json"] = executeResult
				this.ServeJSON()
				return
			}

			//3.删除购物车表
			_, err = Db.Exec("delete from "+shopConfig.TablePre+"cart where id = ? and uid = ?", v.Id, v.Uid)
			if err != nil {
				isExecuteSuccess = false
				conn.Rollback()
				executeResult["status"] = 0
				executeResult["msg"] = "删除购物车表异常" + err.Error()
				this.Data["json"] = executeResult
				this.ServeJSON()
				return
			}

			//4.更新商品表库存量，已出售数量
			_, err = Db.Exec("update "+shopConfig.TablePre+"product set num = num -?,saledcount=saledcount+? where id = ?", v.Num, v.Num, v.Pid)
			if err != nil {
				isExecuteSuccess = false
				conn.Rollback()
				executeResult["status"] = 0
				executeResult["msg"] = "更新商品表库存异常" + err.Error()
				this.Data["json"] = executeResult
				this.ServeJSON()
				return
			}
		}
		if isExecuteSuccess {
			conn.Commit()
		}

	}

	executeResult["status"] = 1
	executeResult["order_id"] = orderid
	executeResult["order_sn"] = currentTime
	this.Data["json"] = executeResult
	this.ServeJSON()
}

func (this *ShopController) GetAuthDataAction() {
	executeResult = make(map[string]interface{})
	code = this.GetString("code")
	if code == "" {
		executeResult["status"] = 0
		executeResult["msg"] = "code异常"
		this.Data["json"] = executeResult
		this.ServeJSON()
		return
	}

	appid = "wx6cd93ed0194fae7f"                   //微信appid
	appsecret = "51612354a087e6eeb7f29103131741d4" //微信secret
	auth_url := "https://api.weixin.qq.com/sns/jscode2session?appid=" + appid + "&secret=" + appsecret + "&js_code=" + code + "&grant_type=authorization_code"

	resp, err := httpsclient.GetData(auth_url)
	if err != nil {

		executeResult["status"] = 0
		executeResult["msg"] = "网络异常" + err.Error()
		this.Data["json"] = executeResult
		this.ServeJSON()
		return
	}

	err = json.Unmarshal(resp, openidAndSessionKey)
	if err != nil {
		executeResult["status"] = 0
		executeResult["msg"] = "网络异常" + err.Error()
		this.Data["json"] = executeResult
		this.ServeJSON()
		return
	}

	executeResult["status"] = 1
	executeResult["msg"] = "获取openid和sessionkey成功" + string(resp)
	executeResult["openid"] = openidAndSessionKey.OpenId
	executeResult["session_key"] = openidAndSessionKey.SessionKey
	this.Data["json"] = executeResult
	this.ServeJSON()
}

func (this *ShopController) UserRegAction() {
	avatarUrl := this.GetString("avatarUrl")
	nickName := this.GetString("nickName")
	openId := this.GetString("openId")
	session_key := this.GetString("sessionKey")

	var integral int64 = 100

	user = nil
	if err = Db.Select(&user, "select id,uname,openid from "+shopConfig.TablePre+"user where openid = ?", openId); err != nil {
		executeResult["status"] = 0
		executeResult["msg"] = "数据库操作异常" + err.Error()
		this.Data["json"] = executeResult
		this.ServeJSON()
		return
	}

	if len(user) == 0 {
		handle, insert_err := Db.Exec("insert into "+shopConfig.TablePre+"user(uname,openid,addtime,integral,photo,session_key) values(?,?,?,?,?,?)", nickName, openId, time.Now().Unix(), integral, avatarUrl, session_key)
		logs.Debug(user)
		logs.Debug(len(user))
		if insert_err != nil {
			executeResult["status"] = 0
			executeResult["msg"] = "注册异常" + err.Error()
			this.Data["json"] = executeResult
			this.ServeJSON()
			return
		} else {
			insertid, err := handle.LastInsertId()
			if err != nil {
				executeResult["status"] = 0
				executeResult["msg"] = "注册异常" + err.Error()
				this.Data["json"] = executeResult
				this.ServeJSON()
				return
			} else {
				userInfo := &conf.User{
					Uname:    nickName,
					Openid:   openId,
					Photo:    avatarUrl,
					Integral: integral,
					Id:       insertid,
				}
				executeResult["status"] = 1
				executeResult["msg"] = "注册成功"
				executeResult["data"] = userInfo
			}
		}
	} else {
		userInfo := &conf.User{
			Uname:    nickName,
			Openid:   openId,
			Photo:    avatarUrl,
			Integral: integral,
			Id:       user[0].Id,
		}

		executeResult["status"] = 1
		executeResult["msg"] = "查询成功"
		executeResult["data"] = userInfo
	}

	this.Data["json"] = executeResult
	this.ServeJSON()

}

//预下单页---击入库并生成支付订单及支付参数
func (this *ShopController) CallbackAction() {
	common.InitMinipay(&common.MiniPayParams{
		AppId:      "wx6cd93ed0194fae7f",
		MchId:      "1483469312",
		Key:        "098F6BCD4621D373CADE4E832627B4F6",
		UnifiedUrl: "https://api.mch.weixin.qq.com/pay/unifiedorder",
	})

	//异步通知的结构体
	var miniPayResult *common.MiniPayAsyncResult

	//通用第一层通信结构体
	var miniPayCommonResult *common.MiniPayCommonResult
	responseBody, _ := ioutil.ReadAll(this.Ctx.Request.Body)
	if miniPayResult, miniPayCommonResult, err = common.Minipay().MiniPayNotifyCallback(this.Ctx.ResponseWriter, responseBody); err != nil {
		fmt.Println("微信响应结果异常----", err.Error())
	}
	fmt.Println("微信响应结果正常------------", miniPayResult)
	fmt.Println("微信响应结果正常------------", miniPayCommonResult)

	//[微信的服务]-》【开发者服务器】-->[解析到MiniPayAsyncResult]-》
	//
	//						  -->[解析到MiniPayAsyncResult]-》	【map】
	//sign = md5(key=value&key=value&secretkey)
	//更改订单状态

	//1.查询订单表中当前订单号是否存在

	//商户订单号	out_trade_no
	//微信支付订单号	transaction_id


	orderSql := fmt.Sprintf("select `id`,`status` from `"+shopConfig.TablePre+"order` where order_sn = %s and status = %d",miniPayResult.OutTradeNo,1)
	if err = Db.Select(&order, orderSql);err != nil {
		fmt.Println("Db.Select err",err.Error())
	}

	fmt.Println(orderSql)



	//2.订单表的支付状态，与更新时间，支付的状态,当前订单的支付状态，1未付款  2待发货 3待收货 4交易完成 5退款退货申请  6退款退货完成或者说订单关闭
	//	//这里要考虑5，6这两种情况 ，不排除非法提交或者测试人员在测试数据时使用到之前的数据来测试。ecSql = fmt.sprintf
	//订单存在才执行后面逻辑
	if len(order) >0 {
		for _,v := range order {
			fmt.Println(v.Id,v.Status)
			//更新
			if _, err = Db.Exec("update "+shopConfig.TablePre+"order set status = ?,trade_no=? where order_sn = ? limit 1", 2, miniPayResult.TransactionId,miniPayResult.OutTradeNo); err != nil {
				executeResult["status"] = 0
				executeResult["msg"] = "操作异常"
				this.Data["json"] = executeResult
				this.ServeJSON()
				return
			}

			fmt.Println()
		}
	}

	//miniPayResult.OutTradeNO

	//3.更新商品表中的库存与已出售数量
	//todo

	//websocket端则从数据库当中去遍历某个用户的某个订单的状态，发生改变则推送给用户

	//拓展思维：我们可以将需要通知给用户的数据写入rabbitmq，由websocket直接从rabbitmq中读取而不是从数据库当中去读取，这样减少了数据库逻辑处理的过程，速度会更快，提高消息通知时间与成功率，具体思路如下：
	//微信服务器将支付结果通知给开发者服务器，还是经过上面的过程，代码如下
	if miniPayResult, miniPayCommonResult, err = common.Minipay().MiniPayNotifyCallback(this.Ctx.ResponseWriter, responseBody); err != nil {
		fmt.Println("微信响应结果异常----", err.Error())
	}
	fmt.Println("微信响应结果正常------------", miniPayResult)
	fmt.Println("微信响应结果正常------------", miniPayCommonResult)
	//成功之后先写到mq当中，则此时另一端的websocket就可以直接读取，然后推送给小程序客户端的用户。之后再走写入数据库的逻辑。
	//这里做为一种扩展思维留给各位同学，同时与我合作的另一位讲师ccmouse，在他的项目当中有讲解到租车项目，他会更加详细的讲解rabbitmq的相关知识，大家可以参照ccmouse老师的rabbitmq相关知识，把我们这里的逻辑改造成mq的逻辑

	this.Data["xml"] = miniPayCommonResult
	this.ServeXML()

}


//从零构建支持测试
func (this *ShopController) MiniPayAction() {

	executeResult = make(map[string]interface{})

	common.InitMinipay(&common.MiniPayParams{
		AppId:      "wx6cd93ed0194fae7f",
		MchId:      "1483469312",
		Key:        "098F6BCD4621D373CADE4E832627B4F6",
		UnifiedUrl: "https://api.mch.weixin.qq.com/pay/unifiedorder",
	})

	payArgs := new(common.PayArgs)
	//payArgs.TradeNum = "boge12312312miniprograme"
	payArgs.TradeNum = fmt.Sprintf("boge%d",time.Now().Unix())
	payArgs.NotifyUrl = "https://luboke.com"
	payArgs.Body = "波哥电商下单"
	payArgs.OpenId = "oR3eB4hDIXDypm-TMVR5bY0uzpfI"
	payArgs.TotalFee = 1

	logs.Debug(common.Minipay())
	logs.Debug(payArgs)
	if unifiedData, err = common.Minipay().UnifiedOrder(payArgs); err != nil {
		executeResult["status"] = 0
		executeResult["msg"] = "统一下单异常 " + err.Error()
		this.Data["json"] = executeResult
		this.ServeJSON()
		return
	}

	executeResult["status"] = 1
	executeResult["msg"] = "统一下单完成"
	executeResult["data"] = unifiedData
	this.Data["json"] = executeResult
	this.ServeJSON()
	return
}


//beego结合sqlx操作数据库的流程
//1.定义和数据库表相对应的结构体，当然不一定要定义所有的字段，具体视业务需求，需要多少定义逃犯
type Product struct {
	Id                int     `db:"id" json:"id,omitempty"`
	Name              string  `db:"name" json:"name,omitempty"`
	Intro             string  `db:"intro" json:"intro,omitempty"`
	PhotoLittle       string  `db:"photo_little" json:"photo_little,omitempty"`
	Price             float64 `db:"price" json:"price,omitempty"`
	PricePreferential float64 `db:"price_preferential" json:"price_preferential,omitempty"`
	SaledCount        int     `db:"saledcount" json:"saledcount,omitempty"`
	IsNew             int     `db:"is_new" json:"is_new,omitempty"`
	IsHot             int     `db:"is_hot" json:"is_hot,omitempty"`
	Content           string  `db:"content" json:"content,omitempty"`
	Num               int     `db:"num" json:"num,omitempty"`
	PhotoLists        string  `db:"photo_lists" json:"photo_lists,omitempty"`
	Cid               int     `db:"cid" json:"cid,omitempty"`
}

//2.定义一个方法来，也就是控制器，这们这里简单些，操作DB的放在action当中来处理，当然我们有了前面web框架的经验，可以把它拆分到MVC当中 ，这里我们就简单些。
func (this *ShopController) DbDealAction() {


	//这里我们定义一个Map来存储响应给客户端的数据，客户端可以是浏览器，也可以是我们的微商城小程序客户端，其实这里我们可以把这个map封装成函数的，那我们这里简单点，直接写在程序当中 ，那各位同学拿到代码之后可以把它们封装成一个函数，这也是检验个位同学对前面知识掌握程度的关键。
	executeResult = make(map[string]interface{})
	//我们定义这个map，存储哪些数据呢，比如我们响应给客户端的数据格式是这样，
	//{
	//	"status":1, 	//当前操作成功或失败，成功1，失败0
	//	"msg":"操作成功",//操作描述
	//	"data":nil,//具体数据
	//}

	//我们来看一看如何获取用户提交的参数
	//我们在Controller中的方法获取
	//GetString(key string) string
	//GetStrings(key string) []string
	//GetInt(key string) (int64, error)
	//GetBool(key string) (bool, error)
	//GetFloat(key string) (float64, error)
	//不管是post还是get的其他方式提交的数据都可以通过上面的方式获取
	//如果在平常开发中肯定设计到和其他语言的对接数据，这个时候对方发过来的数据可能就是一个json格式的数据，如果想要获取可以通过下面方法：
	//直接通过Ctx.Input.Request.Body获取原始的数据
	//配置文件里设置 copyrequestbody = true
	//这样就可以通过this.Ctx.Input.Request.Body 获取


	var pwd int
	var price float64
	var err error
	if pwd,err = this.GetInt("pwd");err != nil {
		fmt.Println("this.GetInt err,",err.Error())
	}

	if price,err = this.GetFloat("price");err != nil {
		fmt.Println("this.GetFloat err,",err.Error())
	}
	//如果form表单请求地址有相同参数则使用url中的，否则使用表单中的
	fmt.Println("username",this.GetString("username"))
	fmt.Println("pwd",pwd)
	fmt.Println("price",price)


	if limit, err = this.GetInt("limit"); err != nil {
		limit = 10
	}

	//这种定义的方式，连续刷新不会追加，可以使用
	//var product []Product

	//以下这种是因为我们定义的是一个全局的，我们访问之后，当前变量的生命周期并没有被销毁，所以当我们去刷新的时候，去追加进去，那有什么办法呢，我们可以在使用之前给他赋值为nil,或者是按照上面方式1来定义，但是我们肯定会定义很多的controller文件，肯定会有重复使用product变量，所以我们把它定义成全局的，在使用之前把它置为nil
	//product                     []conf.Product      //商品相关的切片

	//查询
	if err = Db.Select(&product, "select id,intro,name,photo_little,price,price_preferential,is_new,is_hot,saledcount from "+shopConfig.TablePre+"product order by saledcount desc limit ?", limit); err != nil {
		executeResult["status"] = 0
		executeResult["msg"] = "数据查询异常" + err.Error()
		this.Data["json"] = executeResult
		this.ServeJSON()
		return
	}


	//插入
	//handle, errs := Db.Exec("insert into "+shopConfig.TablePre+"order(uid,order_sn,price,amount,vid,price_real_pay,addtime,receiver,tel,address_detail,remark,product_num) values(?,?,?,?,?,?,?,?,?,?,?,?)", uid, currentTime, price, minusPrice, couponid, minusPrice, currentTime, concat, tel, address_detail, remark, proNum)
	//handle.LastInsertId()//返回上次影响的记录id

	//更新
	//if _, err = Db.Exec("update "+shopConfig.TablePre+"cart set num = ? where uid = ? and pid = ?", newCartNum, uid, pro_id); err != nil {
	//	executeResult["status"] = 0
	//	executeResult["msg"] = "操作异常"
	//	this.Data["json"] = executeResult
	//	this.ServeJSON()
	//	return
	//}

	//删除
	//if _, err = Db.Exec("delete from "+shopConfig.TablePre+"product_collection where uid = ? and pid = ?", uid, pro_id); err != nil {
	//	executeResult["status"] = 0
	//	executeResult["msg"] = "数据库操作异常" + err.Error()
	//	this.Data["json"] = executeResult
	//	this.ServeJSON()
	//	return
	//}

	//关于sqlx的更多使用方式，这里呢有一个官方的文档，不过是英文的，
	//英文的官方网址 http://jmoiron.github.io/sqlx/
	//电子书镜像    https://www.luboke.com/go/go2.16.html
	//那老师呢会把它翻译成中文的，连着这个英文的一起发给各位同学好吧。

	//这里就是我们所输出的json格式，executeResult我们定义的map
	executeResult["status"] = 1
	executeResult["msg"] = "数据获取成功"
	executeResult["data"] = product

	//通过指定this.Data["json"] = xxx，默认成json格式
	this.Data["json"] = executeResult

	//输出
	this.ServeJSON()

	//至此我们接口所涉及到的beego的相关操作知识全在这里了，那关于beego的更多知识，大家可以参照beego官网或者看老师为大家提供的电子书。
	//打开如下网址描述下
	//http://www.luboke.com/go/go2.14.html
}


func (this *ShopController) TestBeego() {

	var names []string = []string{"imooc","波哥","golang"}
	this.Data["json"] = names
	this.ServeJSON()
	return
}