package main

import "github.com/astaxie/beego"
import (
	_ "minishop_imooc_backend/router"
)

func main() {
	//这一版是基于gopath的方式，可以放在gopath/src目录下，当然同学也可以根据课程中所讲解的，将gopath方式改成gomod的方式来改进这一版expor
	beego.Run()
	//合并分支
}
