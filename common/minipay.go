package common

import (
	"bytes"
	"encoding/xml"
	"errors"
	"fmt"
	"github.com/astaxie/beego/logs"
	"io"
	"log"
	"net/http"
	"strings"
	"time"
)

var (
	miniPayParams     *MiniPayParams
	sign              string
	err               error
	appId             string
	payHandle         map[string]interface{}
	signType          string
	tradeType         string
	miniPaySyncResult MiniPaySyncResult
)

func init() {
	payHandle = make(map[string]interface{})
	signType = "MD5"
	tradeType = "JSAPI"
}

//设置参数
func InitMinipay(pay *MiniPayParams) {
	miniPayParams = pay
}

//返回参数
func Minipay() *MiniPayParams {
	return miniPayParams
}

func (this *MiniPayParams) UnifiedOrder(payArgs *PayArgs) (map[string]interface{}, error) {
	payHandle = make(map[string]interface{})
	payHandle["appid"] = this.AppId
	payHandle["mch_id"] = this.MchId
	payHandle["nonce_str"] = GenerateRandomStr()
	payHandle["sign_type"] = signType
	payHandle["body"] = payArgs.Body
	payHandle["out_trade_no"] = payArgs.TradeNum

	payHandle["total_fee"] = fmt.Sprintf("%1.0f", payArgs.TotalFee)
	payHandle["spbill_create_ip"] = "49.234.14.102"
	payHandle["notify_url"] = payArgs.NotifyUrl
	payHandle["trade_type"] = tradeType
	payHandle["openid"] = payArgs.OpenId
	if sign, err = MiniPaySign(this.Key, payHandle); err != nil {
		return payHandle, err
	}

	payHandle["sign"] = sign
	logs.Debug("payHandle---",payHandle)

	if miniPaySyncResult, err = PostMiniPay(this.UnifiedUrl, payHandle); err != nil {
		logs.Debug(err.Error())
		return payHandle, err
	}

	payHandle = make(map[string]interface{})

	payHandle["appId"] = this.AppId
	payHandle["timeStamp"] = fmt.Sprintf("%d", time.Now().Unix())
	payHandle["nonceStr"] = GenerateRandomStr()
	payHandle["package"] = miniPaySyncResult.PrepayId
	payHandle["signType"] = signType
	if sign, err = MiniPaySign(this.Key, payHandle); err != nil {
		return payHandle, err
	}

	payHandle["paySign"] = sign
	return payHandle, nil
}

//签名	sign	是	String(64)	C380BEC2BFD727A4B6845133519F3AD6	通过签名算法计算得出的签名值，详见签名生成算法

// MiniCallback 微信支付
//response是ResponseWriter接口的实现。
//WriteHeader方法：
//设置响应的状态码，返回错误状态码特别有用。WriteHeader方法在执行完毕之后就不允许对首部进行写入了。
//
//向客户端返回JSON数据：
//首先使用Header方法将内容类型设置成application/json，然后将JSON数据写入ResponseWriter中
//go 处理http response
func  (this *MiniPayParams)MiniPayNotifyCallback(w http.ResponseWriter, body []byte) (*MiniPayAsyncResult, *MiniPayCommonResult, error) {
	var returnCode = "FAIL"
	var returnMsg = ""
	var miniPayCommonResult MiniPayCommonResult
	log.Println(w)
	defer func() {
		//formatStr := `<xml><return_code><![CDATA[%s]]></return_code><return_msg>![CDATA[%s]]</return_msg></xml>`
		//returnBody = fmt.Sprintf(formatStr, returnCode, returnMsg)
		log.Println("log.Println(miniPayCommonResult)---before-----", miniPayCommonResult)
		miniPayCommonResult.ReturnCode = returnCode
		miniPayCommonResult.ReturnMsg = returnMsg
		log.Println("log.Println(miniPayCommonResult)---after-----", miniPayCommonResult)

	}()
	var reXML MiniPayAsyncResult

	log.Println(string(body))
	if string(body) == "" {
		returnCode = "FAIL"
		returnMsg = "Bodyerror"
		return nil,nil, errors.New(returnCode + ":" + returnMsg)
	}
	err = xml.Unmarshal(body, &reXML)
	if err != nil {
		returnCode = "FAIL"
		returnMsg = "参数错误"
		return nil,nil, errors.New(returnCode + ":" + returnMsg)
	}

	if reXML.ReturnCode != "SUCCESS" {
		returnCode = "FAIL"
		return nil,nil, errors.New(reXML.ReturnCode)
	}

	log.Println("xml.Unmarshal(body, &reXML)",reXML)



	xmlmap := XmlToMap(body)

	//看一下xmltomap之后再拼接成待签名的字符串
	var signData []string
	for k, v := range xmlmap {
		if k == "sign" {
			continue
		}
		signData = append(signData, fmt.Sprintf("%v=%v", k, v))
	}

	log.Println(xmlmap)
	log.Println(signData)

	log.Println("minipay()----", &Minipay().Key)
	key := Minipay().Key
	log.Println("key------", key)
	mySign, err := MiniPaySign(key, xmlmap)
	if err != nil {
		return nil,nil, err
	}

	if mySign != xmlmap["sign"] {
		panic(errors.New("签名交易错误"))
	}

	returnCode = "SUCCESS"
	returnMsg = "SUCCESS"
	return &reXML, &miniPayCommonResult, nil
}


//xml转map
func XmlToMap(xmlData []byte) map[string]interface{} {
	decoder := xml.NewDecoder(bytes.NewReader(xmlData))
	m := make(map[string]interface{})
	var token xml.Token
	var err error
	var k string
	for token, err = decoder.Token(); err == nil; token, err = decoder.Token() {
		if v, ok := token.(xml.StartElement); ok {
			k = v.Name.Local
			continue
		}
		if v, ok := token.(xml.CharData); ok {
			data := string(v.Copy())
			if strings.TrimSpace(data) == "" {
				continue
			}
			m[k] = data
		}
	}

	if err != nil && err != io.EOF {
		panic(err)
	}
	return m
}