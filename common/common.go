package common

import (
	"bytes"
	"crypto/md5"
	"encoding/xml"
	"fmt"
	"github.com/astaxie/beego/logs"
	"github.com/pkg/errors"
	"log"
	"sort"
	"strings"
	"time"
)

//通用第一层通信结构体
type MiniPayCommonResult struct {
	ReturnCode string `xml:"return_code" json:"return_code"`
	ReturnMsg  string `xml:"return_msg" json:"return_msg"`
}
type MiniPayCommonResult1 struct {
	ReturnCode string `xml:"return_code" json:"return_code"`
	ReturnMsg  string `xml:"return_msg" json:"return_msg"`
}
//通用第二层通信结构体
type MiniPayReturnSuccessData struct {
	AppId      string `xml:"appid" json:"appid"`
	MchId      string `xml:"mch_id" json:"mch_id"`
	DeviceInfo string `xml:"device_info" json:"device_info"`
	NonceStr   string `xml:"nonce_str" json:"nonce_str"`
	Sign       string `xml:"sign" json:"sign"`
	ResultCode string `xml:"result_code" json:"result_code"`
	ErrCode    string `xml:"err_code" json:"err_code"`
	ErrCodeDes string `xml:"err_code_des" json:"err_code_des"`
}

type MiniPayReturnSuccessData1 struct {
	AppId string `xml:"appid" json:"appid"`
	MchId string `xml:"mch_id" json:"mch_id"`
}

//统一下单响应部分
type MiniPayStateData struct {
	TradeType string `xml:"trade_type" json:"trade_type"`
	PrepayId  string `xml:"prepay_id" json:"prepay_id"`
	CodeUrl   string `xml:"code_url" json:"code_url"`
}

//异步通知响应部分
type MiniPayNotifyStateData struct {
	SignType      string `xml:"sign_type" json:"sign_type"`
	OpenId        string `xml:"openid" json:"openid"`
	IsSubscribe   string `xml:"is_subscribe" json:"is_subscribe"`
	TradeType     string `xml:"trade_type" json:"trade_type"`
	BankType      string `xml:"bank_type" json:"bank_type"`
	TotalFee      string `xml:"total_fee" json:"total_fee"`
	cash_fee      string `xml:"cash_fee" json:"cash_fee"`
	TransactionId string `xml:"transaction_id" json:"transaction_id"`
	OutTradeNo    string `xml:"out_trade_no" json:"out_trade_no"`
	Attach        string `xml:"attach" json:"attach"`
	TimeEnd       string `xml:"time_end" json:"time_end"`
}

//统一下单响应的数据的结构体
type MiniPaySyncResult struct {
	MiniPayCommonResult
	MiniPayReturnSuccessData
	MiniPayStateData
}

//异步通知的结构体
type MiniPayAsyncResult struct {
	MiniPayCommonResult
	MiniPayReturnSuccessData
	MiniPayNotifyStateData
}

//微信支付公用参数
type MiniPayParams struct {
	AppId      string //appid
	MchId      string //商户号
	Key        string //密钥key
	UnifiedUrl string //统一下单地址，"https://api.mch.weixin.qq.com/pay/unifiedorder"

}

//调用统一下单的结构体
type PayArgs struct {
	TradeNum  string  `json:"trade_num,omitempty"`
	TotalFee  float64 `json:"total_fee,omitempty"`
	OpenId    string  `json:"openid,omitempty"`
	NotifyUrl string  `json:"notify_url,omitempty"` //小程序支付异步通知地址
	Body      string  `json:"body,omitempty"`
}

//随机字符串函数
func GenerateRandomStr() string {
	return fmt.Sprintf("%d", time.Now().UnixNano())
}

//微信支付签名
func MiniPaySign(secretKey string, payArgs map[string]interface{}) (string, error) {
	var signData []string
	for k, v := range payArgs {
		if v != "" && k != "sign" && k != "key" {
			signData = append(signData, fmt.Sprintf("%s=%s", k, v))
		}
	}
	sort.Strings(signData)
	signStr := strings.Join(signData, "&")
	signStr = signStr + "&key=" + secretKey
	logs.Debug("signStr---",signStr)
	md5Hash := md5.New()
	_, err := md5Hash.Write([]byte(signStr))
	if err != nil {
		return "", errors.New("md5 err " + err.Error())
	}

	signByte := md5Hash.Sum(nil)
	sign = strings.ToUpper(fmt.Sprintf("%x", signByte))
	return sign, nil
}

//调用微信统一下单接口
func PostMiniPay(url string, data map[string]interface{}) (MiniPaySyncResult, error) {
	var miniPaySyncResult MiniPaySyncResult
	buf := bytes.NewBufferString("")

	for k, v := range data {
		buf.WriteString(fmt.Sprintf("<%s><![CDATA[%s]]></%s>", k, v, k))
	}

	postData := fmt.Sprintf("<xml>%s</xml>", buf.String())
	logs.Debug("postData--",postData)
	resultByte, err := HttpsClient.PostData(url, postData, "text/xml;charset=UTF-8")
	log.Println("通过Http客户端发起请求，回返的数据是字节，经过string之后的数据------", string(resultByte))
	if err != nil {
		return miniPaySyncResult, errors.New("向微信发起下单错误" + err.Error())
	}

	if err = xml.Unmarshal(resultByte, &miniPaySyncResult); err != nil {
		return miniPaySyncResult, errors.New("Unmarshal err " + err.Error())
	}

	if miniPaySyncResult.ReturnCode != "SUCCESS" {
		return miniPaySyncResult, errors.New("通信失败" + err.Error())
	}

	if miniPaySyncResult.ResultCode != "SUCCESS" {
		return miniPaySyncResult, errors.New("业务失败" + err.Error())
	}

	return miniPaySyncResult, nil
}
