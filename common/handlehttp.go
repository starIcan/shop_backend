package common

import (
	"github.com/pkg/errors"
	"io/ioutil"
	"net/http"
	"strings"
)

type HTTPSClient struct {
	http.Client
}

type HTTPClient struct {
	http.Client
}

var (
	httpClient  *HTTPClient
	HttpsClient *HTTPSClient
)

func init() {
	HttpsClient = &HTTPSClient{}
}

func (c *HTTPSClient) PostData(url string, data string, contentType string) ([]byte, error) {
	resp, err := c.Post(url, contentType, strings.NewReader(data))
	if err != nil {
		panic(err)
	}

	defer resp.Body.Close()

	return ioutil.ReadAll(resp.Body)

}

func (c *HTTPSClient) GetData(url string) ([]byte, error) {
	resp, err := c.Get(url)

	if err != nil {
		panic(err)
	}

	if resp.StatusCode != 200 {
		return []byte{}, errors.New("http status err")
	}

	defer resp.Body.Close()
	return ioutil.ReadAll(resp.Body)
}
