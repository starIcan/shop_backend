module minishop_imooc_backend

go 1.15


require (
	github.com/astaxie/beego v1.12.3
	github.com/beego/bee v1.12.3
	//github.com/codersay/MiniPay v1.0.0
	github.com/go-sql-driver/mysql v1.6.0
	github.com/jmoiron/sqlx v1.3.4
	github.com/pkg/errors v0.9.1
	github.com/shopspring/decimal v1.2.0 // indirect
)
