package conf

import (
	"fmt"
	"github.com/astaxie/beego"
)

type PhotoList struct {
	Id       int    `db:"id" json:"id,omitempty"`
	Name     string `db:"name" json:"name,omitempty"`
	PhotoBig string `db:"photo_big" json:"photo_big,omitempty"`
}

type Product struct {
	Id                int     `db:"id" json:"id,omitempty"`
	Name              string  `db:"name" json:"name,omitempty"`
	Intro             string  `db:"intro" json:"intro,omitempty"`
	PhotoLittle       string  `db:"photo_little" json:"photo_little,omitempty"`
	Price             float64 `db:"price" json:"price,omitempty"`
	PricePreferential float64 `db:"price_preferential" json:"price_preferential,omitempty"`
	SaledCount        int     `db:"saledcount" json:"saledcount,omitempty"`
	IsNew             int     `db:"is_new" json:"is_new,omitempty"`
	IsHot             int     `db:"is_hot" json:"is_hot,omitempty"`
	Content           string  `db:"content" json:"content,omitempty"`
	Num               int     `db:"num" json:"num,omitempty"`
	PhotoLists        string  `db:"photo_lists" json:"photo_lists,omitempty"`
	Cid               int     `db:"cid" json:"cid,omitempty"`
	BrandId               int     `db:"brand_id" json:"brand_id,omitempty"`
}

//首页----轮播图
type PhotoLists struct {
	Id        int    `json:"id"`        //主键id
	Name      string `json:"name"`      //商品名称
	Photo_big string `json:"photo_big"` //商品大图
}

type SearchList struct {
	Uid     int    `db:"uid" json:"uid,omitempty"`
	Keyword string `db:"keyword" json:"keyword,omitempty"`
	Num     int    `db:"num" json:"num,omitempty"`
}

type ShopConfig struct {
	RedisAddr string
	RedisPort string

	DbDriver   string
	DbUser     string
	DbPwd      string
	DbHost     string
	DbPort     string
	DbDatabase string
	DbProtocol string
	TablePre   string
	NotifyUrl  string
}

type CategoryList struct {
	Id      int    `db:"id" json:"id,omitempty"`
	Tid     int    `db:"tid" json:"tid,omitempty"`
	Name    string `db:"name" json:"name,omitempty"`
	Preview string `db:"preview" json:"preview,omitempty"`
}

type Collection struct {
	Id  int `db:"id" json:"id,omitempty"`
	Pid int `db:"pid" json:"pid,omitempty"`
	Uid int `db:"uid" json:"uid,omitempty"`
}

type Carts struct {
	Id          int     `db:"id" json:"id,omitempty"`
	Pid         int     `db:"pid" json:"pid,omitempty"`
	Uid         int     `db:"uid" json:"uid,omitempty"`
	Num         int     `db:"num" json:"num,omitempty"`
	Price       float64 `db:"price" json:"price,omitempty"`
	PhotoLittle string  `db:"photo_little" json:"photo_little,omitempty"`
	Name        string  `db:"name" json:"name,omitempty"`
}


type CouponDetail struct {
	Id        int     `db:"id" json:"id,omitempty"`
	Title     string  `db:"title" json:"title,omitempty"`
	MuchMoney float64 `db:"much_money" json:"much_money,omitempty"`
	Amount    float64 `db:"amount" json:"amount,omitempty"`
}

type ProductCarts struct {
	Pnum        int     `db:"pnum" json:"pnum,omitempty"`
	Name        string  `db:"name" json:"name,omitempty"`
	PhotoLittle string  `db:"photo_little" json:"photo_little,omitempty"`
	Price       float64 `db:"price" json:"price,omitempty"`
	Uid         int     `db:"uid" json:"uid,omitempty"`
	Num         int     `db:"num" json:"num,omitempty"`
	Pid         int     `db:"pid" json:"pid,omitempty"`
	Id          int     `db:"id" json:"id,omitempty"`
}


//商品详情页-浏览足迹
type ProductView struct {
	Id        int    `json:"id"`        //主键id
	Pid      int `json:"pid"`      //商品id
	Uid      int `json:"uid"`      //用户id
	Num      int `json:"num"`      //浏览次数
	Pname    string `json:"pname"`      //商品名称
	PhotoLittle  string `json:"photo_little" db:"photo_little"`      //商品图片
	Status      int `json:"status"`
}


type Address struct {
	Id            int    `db:"id" json:"id,omitempty"`
	Uid           int    `db:"uid" json:"uid,omitempty"`
	Name          string `db:"name" json:"name,omitempty"`
	Tel           string `db:"tel" json:"tel,omitempty"`
	Province      int    `db:"province" json:"province,omitempty"`
	City          int    `db:"city" json:"city,omitempty"`
	Countryard    int    `db:"countryard" json:"countryard,omitempty"`
	Address       string `db:"address" json:"address,omitempty"`
	AddressDetail string `db:"address_detail" json:"address_detail,omitempty"`
	IsDetail      int    `db:"is_default" json:"is_default,omitempty"`
}

//混合项
//所有数据表共用的，用于统计的总记录数
type TotalRow struct {
	Total int64 `json:"total"`
}

//个人中心-我的收藏
type MyCollect struct {
	Pid                int64     `json:"pid"`
	Uid                int64     `json:"uid"`
	Photo_little       string  `json:"photo_little"`
	Name               string  `json:"name"`
	Price_preferential float64 `json:"price_preferential"`
	Intro              string  `json:"intro"`
}

//省市区
type Map struct {
	Id   int64    `json:"id"`
	Tid  int64    `json:"tid"`
	Name string `json:"name"`
	Code string `json:"code"`
}
//个人中心，优惠券领取
type Coupon struct {
	Id             int     `json:"id"`
	Shop_id        string  `json:"shop_id"`
	Title          string  `json:"title"`
	Much_money     float64 `json:"much_money"`
	Amount         float64 `json:"amount"`
	Start_time     string  `json:"start_time"`
	End_time       string  `json:"end_time"`
	Point          int     `json:"point"`
	Count          int     `json:"count"`
	Get_coupon_num int     `json:"get_coupon_num"`
	Addtime        string  `json:"addtime"`
	Del            int     `json:"del"`
	Proid          string  `json:"proid"`
}

//优惠券
type UserCoupon struct {
	Uid     int64    `json:"uid"`
	Vid     int64    `json:"vid"`
	Addtime string `json:"addtime"`
	Status  int64    `json:"status"`
}

//我的优惠券
type MyUserCoupon struct {
	Title      string  `json:"title"`
	Amount     float64 `json:"amount"`
	Much_money float64 `json:"much_money"`
	Start_time string  `json:"start_time"`
	End_time   string  `json:"end_time"`
	Point      int64     `json:"point64"`
	Status     int64     `json:"status"`
	Proid      string  `json:"proid"`
}

//订单表
type Order struct {
	Id             int64     `json:"id"`
	Order_sn       string  `json:"order_sn"`
	Trade_no       string  `json:"trade_no"`
	Status         int64     `json:"status"`
	Price          float64 `json:"price"`
	Price_real_pay float64 `json:"price_real_pay"`
	Product_num    int64     `json:"product_num"`
	Receiver       string  `json:"receiver"`
	PhotoLittle       string  `json:"photo_little"`
	Address_detail string  `json:"address_detail"`
	Addtime        int64     `json:"addtime"`
	Remark string  `json:"remark"`
}

//订单详情表
type OrderProduct struct {
	Id           int64     `json:"id"`
	Pid          int64     `json:"pid"`
	Order_id     int64     `json:"order_id"`
	Name         string  `json:"name"`
	Price        float64 `json:"price"`
	Photo_little string  `json:"photo_little"`
	Num          int64     `json:"num"`
}

type OpenIdAndSessionKey struct {
	OpenId     string `json:"openid"`
	SessionKey string `json:"session_key"`
}

type User struct {
	Id       int64  `json:"id"`
	Uname    string `json:"uname"`
	Openid   string `json:"openid"`
	Integral int64    `json:"integral"`
	Addtime  int    `json:"addtime"`
	Photo    string `json:"photo"`
}

var shopConfig = &ShopConfig{}

func InitAppConfig() *ShopConfig {
	redisAddr := beego.AppConfig.String("redis_addr")
	redisPort := beego.AppConfig.String("redis_port")
	dbDriver := beego.AppConfig.String("db_driver")
	dbUser := beego.AppConfig.String("db_user")
	dbPwd := beego.AppConfig.String("db_pwd")
	dbHost := beego.AppConfig.String("db_host")
	dbPort := beego.AppConfig.String("db_port")
	dbDatabase := beego.AppConfig.String("db_database")
	dbProtocol := beego.AppConfig.String("db_protocol")
	tablePre := beego.AppConfig.String("table_pre")
	notify_url := beego.AppConfig.String("notify_url")

	shopConfig.RedisAddr = redisAddr
	shopConfig.RedisPort = redisPort
	shopConfig.DbDriver = dbDriver
	shopConfig.DbUser = dbUser
	shopConfig.DbPwd = dbPwd
	shopConfig.DbHost = dbHost
	shopConfig.DbPort = dbPort
	shopConfig.DbDatabase = dbDatabase
	shopConfig.DbProtocol = dbProtocol
	shopConfig.TablePre = tablePre
	shopConfig.NotifyUrl = notify_url
	fmt.Println("hahhahhahahahahahahahha")
	return shopConfig
}
