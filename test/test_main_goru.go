package main

import (
	"fmt"
	"time"
)

func main() {
	go func() {
		fmt.Println("___________________main1_____________")
	}()
	go func() {
		fmt.Println("__________________main2_______________")
	}()
	fmt.Println("_______________main____________")
	time.Sleep(2*time.Second)
}
