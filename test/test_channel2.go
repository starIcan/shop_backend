package main

import (
	"fmt"
	"io/ioutil"
	"math/rand"
	"net/http"
	"strings"
	"sync"
	"time"
)
//任务结构体
type Task struct {
	ID         int
	JobID      int
	Status     string
	CreateTime time.Time
	Urls	   string
	RStatus 	string
	RStatusCode int
}

func (t *Task) run(){
	sleep := rand.Intn(1000)
	time.Sleep(time.Duration(sleep)*time.Millisecond)
	rstatus,rstatuscode := GetData(t.Urls)
	t.Status = "Completed"
	t.RStatus = rstatus
	t.RStatusCode = rstatuscode
}

func GetData(url string) (string, int) {
	resp, err := http.Get(url)
	if err !=nil{
		panic(err)
	}
	defer resp.Body.Close()
	return resp.Status,resp.StatusCode
}

func PostData(url string, data string, contentType string) ([]byte, error) {
	resp,err := http.Post(url, contentType, strings.NewReader(data))
	if err != nil {
		panic(err)
	}

	defer resp.Body.Close()
	return ioutil.ReadAll(resp.Body)
}

var wg sync.WaitGroup

const  workernum = 3

func main() {
	wg.Add(workernum)
	taskQueue := make(chan *Task,10)
	for workID := 1;workID<=workernum;workID++{
		go worker(taskQueue,workID)
	}
	for i := 1; i <= 15; i++ {
		taskQueue <- &Task{
			ID:         i,
			JobID:      100 + i,
			CreateTime: time.Now(),
			Urls:"http://www.luboke.com",
		}
	}
	close(taskQueue)
	wg.Wait()
}

func worker(in <-chan *Task, workID int) {
	defer wg.Done()
	for v := range in {
		fmt.Printf("Worker-%d: 处理请求【TaskID】:%d, 【JobID】:%d\n", workID, v.ID, v.JobID)
		v.run()
		fmt.Printf("Worker-%d: 完成请求【TaskID】:%d, 【JobID】:%d,【状态描述】:%s,【状态码】:%d\n", workID, v.ID, v.JobID,v.RStatus,v.RStatusCode)
	}
}