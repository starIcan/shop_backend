package main

import (
	"fmt"
	"time"
)

func main() {

	ch := make(chan string)
	go sender(ch)
	go recever(ch)

	//注释就不会执行
	time.Sleep(1e9)
}

func recever(ch chan string) {
	//var recv string

	for {
		val, ok := <-ch
		if ok {
			fmt.Println(val)
		}
		//recv = <-ch
		//fmt.Println(recv)
	}
}

func sender(ch chan string) {
	ch <- "aa"
	ch <- "bb"
	ch <- "cc"
	close(ch)
	ch <- "dd"
}
