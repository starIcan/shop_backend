package main

import (
	"fmt"
	"time"
)

func main() {
	ch1 := make(chan int)
	ch2 := make(chan int)
	go put1(ch1)
	go put2(ch2)
	go suck(ch1,ch2)
	time.Sleep(1e9)
	

}

func suck(ch1 chan int, ch2 chan int) {
	for{
		select {
			case v := <-ch1:
				fmt.Printf("Recv on ch1: %d\n", v)
			case v := <-ch2:
				fmt.Printf("Recv on ch2: %d\n", v)
		}

	}
}

func put2(ch chan int) {
	for i := 0; i <= 30; i++ {
		if i%2 == 1 {
			ch <- i
		}
	}
}

func put1(ch chan int) {
	for i := 0; i <= 30; i++ {
		if i%2 == 0 {
			ch <- i
		}
	}
}
