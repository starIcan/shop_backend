package main

import (
	"fmt"
	"time"
)

func main() {
	tick := time.Tick(1 * time.Second)
	after := time.After(7 * time.Second)
	fmt.Println("start second:",time.Now().Second())
	for {
		select {
		case <-tick:
			fmt.Println("1 second over:", time.Now().Second())
		case <-after:
			fmt.Println("7 second over:", time.Now().Second())
			return
		}
	}
}