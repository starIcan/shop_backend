package main

import "fmt"
type userinfo struct {
	name string
	age int
	height float32
	eduschool string
	hobby []string
	moreinfo map[string]interface{}
}


func main() {
	user1 := userinfo{
		name:      "user1",
		age:       0,
		height:    0,
		eduschool: "",
		hobby:     nil,
		moreinfo:  nil,
	}
	user2 := userinfo{
		name:      "user2",
		age:       0,
		height:    0,
		eduschool: "",
		hobby:     nil,
		moreinfo:  nil,
	}
	user3 := user2
	fmt.Printf("user1 = %p,user2 = %p\n",user1,&user2)
	fmt.Println(&user3.name)
}


