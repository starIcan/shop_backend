
package main

import (
"fmt"
"math/rand"
"time"
)

// 不断向channel c中发送[0,10)的随机数
func send(c chan int) {
	for {
		c <- rand.Intn(10)
	}
}

func add(c chan int) {
	sum := 0

	// 1秒后，将向t.C通道发送时间点，使其可读
	t := time.NewTimer(1 * time.Second)

	for {
		// 一秒内，将一直选择第一个case
		// 一秒后，t.C可读，将选择第二个case
		// c变成nil channel后，两个case分支都将一直阻塞
		select {
		case input := <-c:
			// 不断读取c中的随机数据进行加总
			sum = sum + input
		case <-t.C:
			c = nil
			fmt.Println(sum)
		}
	}
}

func main() {
	c := make(chan int)
	go add(c)
	go send(c)
	// 给3秒时间让前两个goroutine有足够时间运行
	time.Sleep(3 * time.Second)
}
