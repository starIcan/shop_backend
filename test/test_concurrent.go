package main

import (
	"fmt"
	"runtime"

	"sync"
)

func main() {
	//runtime.GOMAXPROCS(1)

	runtime.GOMAXPROCS(2)
	var wg sync.WaitGroup
	wg.Add(2)

	fmt.Println("Start Goroutines")

	go func() {
		defer wg.Done()

		for count := 0;count<2;count++{
			for char := 'a';char<'a'+26;char++{
				fmt.Printf("%c ",char)
			}
		}
	}()

	go func() {
		// 在函数退出时调用 Done 来通知 main 函数工作已经完成
		defer wg.Done()

		// 显示字母表 2 次
		for count := 0; count < 2; count++ {
			for char := 'A'; char < 'A'+26; char++ {
				fmt.Printf("%c ", char)
			}
		}
	}()

	// 等待 goroutine 结束
	fmt.Println("Waiting To Finish")

	//一旦两个匿名函数创建 goroutine 来执行，main 中的代码会继续运行。
	//这意味着 main 函数会在 goroutine 完成工作前返回。如果真的返回了，程序就会在 goroutine 有
	//机会运行前终止。因此，在 main 函数通过 WaitGroup，等待两个 goroutine 完成它们 的工作。
	wg.Wait()

	fmt.Println("\nTerminating Program")
}
